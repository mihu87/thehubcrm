#!/usr/bin/php
<?php

function get_sku_info($sku_code) {
    global $mysqli;
    if ($result = $mysqli->query("select * from skus where sku_code = '$sku_code'")) {
	if ($result->num_rows) {
	    return $result->fetch_object();
	} else {
	    return false;
	}
    }
    return false;
}

function process() {
    global $mysqli;


    $r_i_q = "select * from raw_interactions where processed=0 order by timestamp asc";
    $r_i_res = $mysqli->query($r_i_q);
    if ($r_i_res === false) {
	echo "Error: SQL: $r_i_q: ".$mysqli->error."\n";
	exit;
    }

    while($int = $r_i_res->fetch_object()) {
	$sku_code = $int->sku_code_1;
	//get sku info
	$__sku_info = get_sku_info($sku_code);
	$sku_name = '';
	$sku_brand = '';
	$sku_vendor = '';
	$sku_manufacturer = '';
	if ($__sku_info === FALSE) {
	    echo "SKU code not found: $sku_code\n";
	} else {
	    $sku_name = $__sku_info->sku_name;
	    $sku_brand = $__sku_info->sku_brand;
	    $sku_vendor = $__sku_info->sku_vendor;
	    $sku_manufacturer = $__sku_info->sku_manufacturer;
	}


	//check for a consumer, based on unique criteria
	$_fname = addslashes($int->fname);
	$_lname = addslashes($int->lname);
	$cons_res = $mysqli->query("select * from consumers where fname='$_fname' and lname='$_lname' and birthdate='{$int->birthdate}' and mobile = '{$int->mobile}'");
	if ($cons_res === false) {
	    echo "Error: SQL: ".$mysqli->error."\n";
	    exit;
	}



	if ($cons_res->num_rows) {
	    //------------------------------------------ existing consumer
	    $consumer = $cons_res->fetch_object();
	    $consumer_id = $consumer->consumer_id;

/*
	    //here we should check if an interaction already exists
	    $check_res = $mysqli->query("select * from consumers_history where consumer_id = $consumer_id and timestamp = '{$int->timestamp}'");
	    if ($check_res === false) {
		echo "Error: SQL: ".$mysqli->error."\n";
		exit;
	    }
	    if ($check_res->num_rows) {
		echo "already processed\n";
		continue;
	    }
*/

	    //-------------------------------------------------------------------------
	    //check brand
	    if($consumer->sku_code != $sku_code) {
		$b_c_query = "update consumers set sku_code='$sku_code', sku_name='$sku_name', sku_brand='$sku_brand', sku_vendor='$sku_vendor', sku_manufacturer='$sku_manufacturer' where consumer_id=$consumer_id";
		$b_res = $mysqli->query($b_c_query);
		if ($b_res === false) {
		    echo "Error: SQL: $b_c_query: ".$mysqli->error."\n";
		    exit;
		}
		//update history
		$b_h_query  = "insert into consumers_history(consumer_id,timestamp,campaign_id,campaign_name,campaign_channel,campaign_sub_channel,";
		$b_h_query .= "prev_sku_code, prev_sku_name, prev_sku_brand, prev_sku_vendor, prev_sku_manufacturer,";
		$b_h_query .= "crt_sku_code, crt_sku_name, crt_sku_brand, crt_sku_vendor, crt_sku_manufacturer,";
		$b_h_query .= "int_type, int_prize, int_person_code, int_pos_code) values(";
		$b_h_query .= "'$consumer_id', '{$int->timestamp}', '{$int->campaign_id}', '{$int->campaign_name}', '{$int->campaign_channel}', '{$int->campaign_sub_channel}',";
		$b_h_query .= "'{$consumer->sku_code}', '{$consumer->sku_name}', '{$consumer->sku_brand}', '{$consumer->sku_vendor}', '{$consumer->sku_manufacturer}',";
		$b_h_query .= "'$sku_code', '$sku_name', '$sku_brand', '$sku_vendor', '$sku_manufacturer',";
		$b_h_query .= "'BRAND_CHANGE', '{$int->int_prize}', '{$int->int_person_code}', '{$int->int_pos_code}')";
		$h_res = $mysqli->query($b_h_query);
		if ($h_res === false) {
		    echo "Error: SQL: $b_h_query: ".$mysqli->error."\n";
		    exit;
		}
	    }

	    //-------------------------------------------------------------------------
	    //check email
	    if ($int->email != $consumer->email && filter_var($int->email, FILTER_VALIDATE_EMAIL)) {
		$email = addslashes($int->email);
		$e_c_query = "update consumers set email='$email' where consumer_id=$consumer_id";
		$e_res = $mysqli->query($e_c_query);
		if ($e_res === false) {
		    echo "Error: SQL: $e_c_query: ".$mysqli->error."\n";
		    exit;
		}

		//history
		$e_h_query  = "insert into consumers_history(consumer_id, timestamp, campaign_id, campaign_name, campaign_channel, campaign_sub_channel,";
		$e_h_query .= "prev_email,";
		$e_h_query .= "crt_email,";
		$e_h_query .= "int_type, int_prize, int_person_code, int_pos_code) values(";
		$e_h_query .= "'$consumer_id', '{$int->timestamp}', '{$int->campaign_id}', '{$int->campaign_name}', '{$int->campaign_channel}', '{$int->campaign_sub_channel}',";
		$e_h_query .= "'{$consumer->email}',";
		$e_h_query .= "'$email',";
		$e_h_query .= "'EMAIL_CHANGE', '{$int->int_prize}', '{$int->int_person_code}', '{$int->int_pos_code}')";

		$h_res = $mysqli->query($e_h_query);
		if ($h_res === false) {
		    echo "Error: SQL: $e_h_query: ".$mysqli->error."\n";
		    exit;
		}

	    }

	    //-------------------------------------------------------------------------
	    //update lastseen, optin
	    $update_query="update consumers set last_seen = '{$int->timestamp}', optin = '{$int->agreement}' where consumer_id=$consumer_id";
	    $update_res = $mysqli->query($update_query);
	    if ($update_res === false) {
		echo "Error: SQL: $update_query: ".$mysqli->error."\n";
		exit;
	    }

	} else {
	    //new consumer
	    //check if the mobile exists if mobile not empty
	    if ($int->mobile != '') {
		$m_c_q="select * from consumers where mobile='{$int->mobile}' and last_seen < '{$int->timestamp}'";
		$m_c_res = $mysqli->query($m_c_q);
		while($t_cons = $m_c_res->fetch_object()) {
		    //delete from history
		    $mysqli->query("delete from consumer_history where consumer_id={$t_cons->consumer_id}");
		    $mysqli->query("delete from consumers where consumer_id={$t_cons->consumer_id}");
		}
	    }

	    // if yes and last_seen < interaction timestamp => delete old records from consumers and history + insert new record
	    // if no, insert new
	    
	    
	    
	    //echo "new consumer: {$int->fname}/{$int->lname}/{$int->birthdate}/{$int->mobile}\n";
	    //add to consumer table
	    $email = addslashes($int->email);
	    $street = addslashes($int->street);
	    $city = addslashes($int->city);
	    $fname = addslashes($int->fname);
	    $lname = addslashes($int->lname);
	    $no = addslashes($int->no);
	    $building = addslashes($int->building);
	    $c_c_query  = "insert into consumers(fname, lname, birthdate, email, mobile,";
	    $c_c_query .= "sku_code, sku_name, sku_brand, sku_vendor, sku_manufacturer,";
	    $c_c_query .= "county, city, sub_city, sub_city_type, street, no, building, entrance, floor, apartment,";
	    $c_c_query .= "last_seen, first_seen, optin) values(";
	    $c_c_query .= "'$fname', '$lname', '{$int->birthdate}', '$email', '{$int->mobile}',";
	    $c_c_query .= "'$sku_code', '$sku_name', '$sku_brand', '$sku_vendor', '$sku_manufacturer',";
	    $c_c_query .= "'{$int->county}','$city', '', '', '$street', '$no', '$building', '{$int->entrance}', '', '{$int->apartment}',";
	    $c_c_query .= "'{$int->timestamp}', '{$int->timestamp}', '{$int->agreement}')";

	    $c_res = $mysqli->query($c_c_query);
	    if ($c_res === false) {
		echo "Error: SQL: $c_c_query: ".$mysqli->error."\n";
		exit;
	    }

        echo "new consumer;" . $mysqli->insert_id . "\n";


	    $consumer_id = $mysqli->insert_id;
	    $cons_res = $mysqli->query("select * from consumers where consumer_id = $consumer_id");
	    if ($cons_res === false) {
		echo "Error: SQL: ".$mysqli->error."\n";
		exit;
	    }
	    $consumer = $cons_res->fetch_object();


	    //add something to history
	    $n_h_query  = "insert into consumers_history(consumer_id,timestamp,campaign_id,campaign_name,campaign_channel,campaign_sub_channel,";
	    //$n_h_query .= "prev_sku_code, prev_sku_name, prev_sku_brand, prev_sku_vendor, prev_sku_manufacturer,";
	    $n_h_query .= "crt_sku_code, crt_sku_name, crt_sku_brand, crt_sku_vendor, crt_sku_manufacturer,";
	    $n_h_query .= "int_type, int_prize, int_person_code, int_pos_code) values(";
	    $n_h_query .= "'$consumer_id', '{$int->timestamp}', '{$int->campaign_id}', '{$int->campaign_name}', '{$int->campaign_channel}', '{$int->campaign_sub_channel}',";
	    //$n_h_query .= "'{$consumer->sku_code}', '{$consumer->sku_name}', '{$consumer->sku_brand}', '{$consumer->sku_vendor}', '{$consumer->sku_manufacturer}',";
	    $n_h_query .= "'$sku_code', '$sku_name', '$sku_brand', '$sku_vendor', '$sku_manufacturer',";
	    $n_h_query .= "'NEW', '{$int->int_prize}', '{$int->int_person_code}', '{$int->int_pos_code}')";
	    $h_res = $mysqli->query($n_h_query);
	    if ($h_res === false) {
		echo "Error: SQL: $n_h_query: ".$mysqli->error."\n";
		exit;
	    }
	}

	//record this interaction for this consumer
	$i_h_query  = "insert into consumers_history(consumer_id, timestamp, campaign_id, campaign_name, campaign_channel, campaign_sub_channel,";
	$i_h_query .= "int_type, int_prize, int_person_code, int_pos_code) values(";
	$i_h_query .= "'$consumer_id', '{$int->timestamp}', '{$int->campaign_id}', '{$int->campaign_name}', '{$int->campaign_channel}', '{$int->campaign_sub_channel}',";
	$i_h_query .= "'INTERACTION', '{$int->int_prize}', '{$int->int_person_code}', '{$int->int_pos_code}')";

	$i_res = $mysqli->query($i_h_query);
	if ($i_res === false) {
	    echo "Error: SQL: $i_h_query: ".$mysqli->error."\n";
	    exit;
	}

	//mark interaction as processed
	$r_i_uq = "update raw_interactions set processed=1 where id={$int->id}";
	$r_i_ures = $mysqli->query($r_i_uq);
	if ($r_i_ures === false) {
	    echo "Error: SQL: $r_i_uq: ".$mysqli->error."\n";
	    exit;
	}

	echo "procs\n";


    }
}


//connect to mysql
$mysqli = new mysqli("localhost", "crmdb", "crmdb", "crmdb");
if ($mysqli->connect_errno) {
    echo "Error: Failed to connect to MySQL: " . $mysqli->connect_error;
    exit;
}

process();

exit;
