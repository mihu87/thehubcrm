<?php

require_once __DIR__ . '/vendor/autoload.php';

use Services\DataContext;

$input = stream_get_contents(STDIN);
$files = unserialize(base64_decode($input));

$context = new DataContext;
$context->setConsole(new \Symfony\Component\Console\Output\ConsoleOutput());
$context->setSource('rsdmf121');

foreach ($files as $record) {

    $outResult = $context->process($record);
    var_dump($outResult);

}


