<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" encoding="iso-8859-1"/>
    <xsl:strip-space elements="*" />

    <xsl:template match="/campaign/consumer">
        <xsl:number value="0"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="@id"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="@time"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="fname"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="lname"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="cnp"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="sex"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="born"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="address/state"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="address/city"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="address/street"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="address/no"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="address/building"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="address/stair"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="address/floor"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="address/ap"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="mobile"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="phone"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="email"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="brand1"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="brand2"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="prize"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="matrix"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="agreement"/>
            <xsl:text>|</xsl:text>
        <xsl:for-each select="questions/question">
            <xsl:text>(</xsl:text>
                <xsl:value-of select="title"/>
                    <xsl:text>/</xsl:text>
                <xsl:value-of select="answer"/>
            <xsl:text>)</xsl:text>
        </xsl:for-each>
        <xsl:text>|</xsl:text>
        <xsl:value-of select="pos/pos_code"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="pos/pos_name"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="pos/pos_city"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="ba"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="/campaign/@id"/>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="/campaign/@name"/>
            <xsl:text>|</xsl:text>
        <xsl:text>121</xsl:text>
            <xsl:text>|</xsl:text>
        <xsl:value-of select="/campaign/@channel"/>
            <xsl:text>|</xsl:text>
        <xsl:number value="0"/>
             <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    <!-- etc -->
</xsl:stylesheet>