<?php

namespace Services;

class DataContext {
    /**
     * @var array
     * A set of key => accessor => (options: ClassName) of our defined sources
     */
    protected $sources = [

        'DMFConsumers' => 'Services\Sources\DMFConsumers\Consumers',
        'RSDMF121' => 'Services\Sources\RSDMF121\DMF121',

        'RSDMSTATUS' => 'Services\Sources\RSDMSTATUS\DMSTATUS',
        'RSDMCoresp' => 'Services\Sources\RSDMCoresp\DMCoresp',
        'RSDMSelection' => 'Services\Sources\RSDMSelection\DMSelection',

        'RSSMSOut' => 'Services\Sources\RSSMSOut\SMSOut',
        'RSSMSCoresp' => 'Services\Sources\RSSMSCoresp\SMSCoresp',
        'RSSMSSelection' => 'Services\Sources\RSSMSSelection\SMSSelection',

        'RSEMClick' => 'Services\Sources\RSEMClick\EMClick',
        'RSEMCoresp' => 'Services\Sources\RSEMCoresp\EMCoresp',
        'RSEMOpen' => 'Services\Sources\RSEMOpen\EMOpen',
        'RSEMOut' => 'Services\Sources\RSEMOut\EMOut',
        'RSEMSelection' => 'Services\Sources\RSEMSelection\EMSelection',


    ];


    protected $console;
    protected $source;
    protected $sourceName;

    /**
     * @param $console
     */
    public function setConsole($console) {
        $this->console = $console;
    }

    /**
     * @param $source
     */
    public function setSource($source) {

        if (!array_key_exists($source, $this->sources)) {
            trigger_error("Source is not defined or probably there is a typo in source name for : {$source}", E_USER_WARNING);
        }

        if (empty($this->sources[$source])) {
            trigger_error("Source class is not defined for : {$source}", E_USER_WARNING);
        }

        if (!class_exists($this->sources[$source])) {
            trigger_error("Unable to load source config class for : {$source} [{$this->sources[$source]}]", E_USER_WARNING);
        }

        $this->source =  new $this->sources[$source];
    }

    public function getSourceType() {
        return $this->source->type;
    }

    public function getSourceRawTableName() {
        return $this->source->rawTable;
    }

    public function download() {
        return $this->source->download();
    }

    public function process($record) {

        $processed = $this->source->process($record);
        return $processed;
    }

    public function processIn($record) {
        $processed = $this->source->processIn($record);
        return $processed;
    }

    public function getSources() {
        return $this->sources;
    }
    /**
     * @return array
     */
    protected function getValidatedSources() {

        $validSources = [];

        foreach($this->sources as $key => $source) {
            if (!empty($this->sources[$key]) && class_exists($source)) {
                $validSources[] = $source;
            }
        }

        return $validSources;
    }

    /**
     * @return bool|\Illuminate\Console\Command
     */
    protected function getLogger() {
        if ($this->console instanceof \Illuminate\Console\Command) {
            return $this->console;
        }
        return false;
    }

}