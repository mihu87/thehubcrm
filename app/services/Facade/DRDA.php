<?php

namespace Services\Facade;

class DRDA extends \Illuminate\Support\Facades\Facade {

    public static function getFacadeAccessor() { return 'drda'; }

}