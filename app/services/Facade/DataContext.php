<?php

namespace Services\Facade;

class DataContext extends \Illuminate\Support\Facades\Facade {

    public static function getFacadeAccessor() { return 'datacontext'; }

}