<?php

namespace Services;

class DRDAServiceProvider extends \Illuminate\Support\ServiceProvider {

    public function register() {
        $this->app->bind('drda', function(){
            return new \Services\DRDA;
        });
    }
}