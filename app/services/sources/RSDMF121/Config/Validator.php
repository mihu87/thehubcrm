<?php

namespace Services\Sources\RSDMF121\Config;

use Services\Sources\Base\BaseValidator;

class Validator extends BaseValidator {

    protected $validationRules = [
        'cnp' => '\CHelpers\Validators\String::checkCNP',
       // 'email' => '\CHelpers\Validators\String::emailApiValidation',
       // 'phone' => '\CHelpers\Validators\String::phoneNumberApiValidation',
    ];

}