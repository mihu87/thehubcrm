<?php

namespace Services\Sources\RSDMF121\Config;

use Services\Sources\Base\BaseNormalizer;

class Normalizer extends BaseNormalizer {

    protected $strict = false;

    protected $additionalFields = [
        'junk' => false,
        'new' => false,
        'valid' => true,
        'invalid_fields' => []
    ];

    protected $normalizationRules = [
        'strtolower',
        'trim',
        'first_name' => '\CHelpers\Normalizers\String::convertFromUtf8ToAsciiTranslit',
        'last_name' => '\CHelpers\Normalizers\String::convertFromUtf8ToAsciiTranslit',
        'city' => '\CHelpers\Normalizers\String::convertFromUtf8ToAsciiTranslit',
        'street_name' => '\CHelpers\Normalizers\String::convertFromUtf8ToAsciiTranslit',
        'birth_date' => '\CHelpers\Normalizers\String::convertBirthDateToTimestamp',
        'optin_optout' => '\CHelpers\Normalizers\String::convertTextToBoolean',

    ];




}