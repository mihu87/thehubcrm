<?php

namespace Services\Sources\RSDMF121\Config;

use Services\Sources\Base\BaseEvaluator;

class Evaluator extends BaseEvaluator  {

    protected $uniqueFields = ['first_name', 'last_name', 'birth_date', 'mobile'];
    protected $uuidColumn   = 'uuid';

    protected $junkRules = [
        '\CHelpers\Validators\Record::hasAllDefinitionFieldsEmpty',
    ];
}