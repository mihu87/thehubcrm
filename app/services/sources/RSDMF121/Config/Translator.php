<?php

namespace Services\Sources\RSDMF121\Config;

use Services\Sources\Base\BaseTranslator;

class Translator extends BaseTranslator {

    /**
     * @var array Defines the field mapper for source -> our DB
     * should be a line that will specify 'origin' => 'destination_field'
     * The engine will check if the origin key exists and it will append the value to destination property
     * If the origin key doesn't exists or is empty , and it has a destination key , that key will become NULL
     * Same if the origin/destination is unspecified, the key will be blank and will exist.
     */

    protected $translationMap = [
        
        'fname' => 'last_name',
        'lname' => 'first_name',
        'sex' => 'gender',
        'born' => 'birth_date',
        'address_state' => 'county',
        'address_city' => 'city',
        'address_street' => 'street_name',
        'address_no' => 'street_no',
        'address_building' => 'building',
        'address_stair' => 'entrance',
        'address_floor' => 'floor',
        'address_ap' => 'apartment',
        'cnp' => 'cnp',
        'mobile' => 'mobile',
        'phone' => 'phone',
        'email' => 'email',
        'brand1' => 'brand_regular',
        'brand2' => 'brand_occasional',
        'prize' => 'prize',
        //'matrix' => '',
        'agreement' => 'optin_optout',
        'pos_pos_code' => 'pos_code',
        //'pos.pos_name' => '',
        'pos_pos_city' => 'pos_city',
        'ba' => 'ba_id',
        'consumer_id' => 'external_id',
        'consumer_time' => 'timestamp',
        'campaign_id' => 'campaign_code',
        'campaign_name' => 'campaign_name',
        'campaign_channel' => 'communication_channel',
        'campaign_sub_channel' => 'communication_sub_channel',
        'source' => 'source'
    ];

}