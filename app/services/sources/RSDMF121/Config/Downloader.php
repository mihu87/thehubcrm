<?php

namespace Services\Sources\RSDMF121\Config;

use Services\Sources\Base\BaseDownloader;


use JMS\Serializer\DeserializationContext;
use Symfony\Component\Console\Output\ConsoleOutput;
use RawInteractions121;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Process\Process;


class Downloader extends BaseDownloader  {

    const FILES_DIR = './app/external/xml/';
    const FILES_PATTERN = '*.xml';
    const CHUNK_SIZE = 50;

    protected $source = 'RSDMF121';

    public function download() {

        /*$p = 0;

        $files = array_chunk(glob(self::FILES_DIR . self::FILES_PATTERN), self::CHUNK_SIZE);

        foreach ($files as $chunk) {

           $str = base64_encode(serialize($chunk));
           $out = `echo $str | php -f send.php > /dev/null &`;
           echo "Chunk {$p} done \n";

           $p++;
        }*/

        // we need to ensure that data is added in raw tables.

        $finder = new Finder();
        $finder->files()->in('./app/external/xml')->name('*.xml');
        $progress = 0;

        foreach ($finder as $file) {

            $this->processXmlFile($file);

            echo $file->getFileName() . " sent to stage 1 ( Progress : $progress ) \n";

            $progress++;
        }

        /**
         * After all files were finished sequentially, we merge all into one csv file
         *
         */

        $mergeCsv = new Process('cat $( ls -1 ./app/external/temp_csv/*.csv | sort -n  ) > ./app/external/import-sql.csv');
        $loadIn = new Process("mysql -e \"load data local infile './app/external/import-sql.csv' into table raw_interactions_121 fields TERMINATED BY '|' LINES TERMINATED BY '\n'\"  -uroot -p123root456 -D crm_dev --local-infile");
        $clean = new Process("rm -rf ./app/external/temp_csv/*");

        $mergeCsv->mustRun();

        if ($mergeCsv->isSuccessful()) {
            $loadIn->setTimeout(300);
            $loadIn->mustRun();
            if ($loadIn->isSuccessful()) {
                $clean->mustRun();

                if ($clean->isSuccessful()) {
                    return true;
                }
            }
        }

        return false;
    }


    protected function processXmlFile($file)
    {
        $name = $file->getFileName();
        $temporary = $name . '.csv';

        $content = str_replace('&', '-', $file->getContents());
        $content = str_replace(',', '', $content);
        $content = utf8_encode($content);

        file_put_contents('./app/external/temp_xml/' . $name, $content, FILE_BINARY);

        $shellProcess = new Process('xsltproc ./app/external/xslt/consumer.xsl ./app/external/temp_xml/' . $name .  ' >> ./app/external/temp_csv/' . $temporary);
        $shellProcess->run();

        if ($shellProcess->isSuccessful()) {
            unlink ('./app/external/temp_xml/' . $name);
        } else {
            throw new \RuntimeException($shellProcess->getErrorOutput());
        }

        echo $shellProcess->getOutput();
    }


}
