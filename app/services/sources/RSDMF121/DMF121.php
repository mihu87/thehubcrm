<?php

namespace Services\Sources\RSDMF121;

use Services\Sources\Source;

class DMF121 extends Source {
    public $type = 'source';
    public $rawTable = 'raw_interactions_121';
}