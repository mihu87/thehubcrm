<?php

namespace Services\Sources\RSDMSTATUS\Config;

use Services\Sources\Base\BaseDownloader;
use Illuminate\Support\Collection;
use PHPExcel_IOFactory;
use RawDmStatus;

class Downloader extends BaseDownloader  {

    protected $type;

    public function download() {

        $file = './app/external/in/TH40_DM status.xlsx';

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($file);

        $lines =  $objPHPExcel->getActiveSheet()->toArray();

        $toAdd = [];

        for ($i=1; $i<count($lines); ++$i) {
            $toAdd[] = array_combine($lines[0], $lines[$i]);
        }

        RawDmStatus::insert($toAdd);
    }
}