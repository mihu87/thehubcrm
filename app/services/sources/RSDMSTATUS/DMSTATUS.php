<?php

namespace Services\Sources\RSDMSTATUS;

use Services\Sources\Source;
use Consumer;
use Illuminate\Support\Facades\DB;
use Interaction;
use Carbon\Carbon;

class DMSTATUS extends Source {

    public $type = 'selection';
    public $rawTable = 'raw_data_dmstatus';

    public function processIn($record) {

        $cData  = DB::table('consumers')->where('mobile', '=', $record->consumer_id)->first();

        if (count($cData)) {

            $int = Interaction::where('uuid', '=', $cData->uuid)->orderBy('timestamp', 'DESC')->first();

            $carbon = new Carbon($record->timestamp);

            $int->purge('id');
            $int->interaction_type = 'DM_OUT';
            $int->dm_status = strtolower($record->status);
            $int->campaign_code = $record->campaign_id;
            $int->timestamp = $carbon->endOfDay()->format('Y-m-d H:i:s');
            $int->timestamp_unix = $carbon->endOfDay()->getTimestamp();
            $int->exists = false;
            $int->save();

            $consumer = Consumer::find($cData->id);
            $consumer->dm_status = strtolower($record->status);
            $consumer->save();

        }
    }
}