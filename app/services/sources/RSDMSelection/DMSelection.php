<?php

namespace Services\Sources\RSDMSelection;

use Services\Sources\Source;
use Consumer;
use Illuminate\Support\Facades\DB;
use Interaction;
use Carbon\Carbon;
use RawDmSelection;

class DMSelection extends Source {

    public $type = 'selection';
    public $rawTable = 'raw_data_dm_selection';

    public function processIn($record) {

       /* $consumer  = DB::table('consumers')->where('mobile', '=', $record->consumer_id)->pluck('uuid');

        if (count($consumer)) {

            $int = Interaction::where('uuid', '=', $consumer)->orderBy('timestamp', 'DESC')->first();

            $carbon = new Carbon($record->timestamp);

            $int->purge('id');
            $int->interaction_type = 'DM_SELECTION';
            $int->campaign_code = $record->campaign_id;
            $int->timestamp = $carbon->endOfDay()->format('Y-m-d H:i:s');
            $int->timestamp_unix = $carbon->endOfDay()->getTimestamp();
            $int->exists = false;
            $int->save();

        }*/

        //var_dump($record);

       /* var_dump($record->uhash1);
        var_dump($record->uhash2);
        var_dump($record->consumer_id);*/

        /*$rst = Consumer::select(DB::raw('CONCAT(mobile,first_name,last_name) AS uhash1'), 'id')->where(DB::raw('CONCAT(mobile,first_name,last_name)'), $record->uhash1)->first()->pluck('uuid');

        if (!count($rst)) {
            $rst = Consumer::select(DB::raw('CONCAT(mobile,last_name) AS uhash2'), 'id')->where(DB::raw('CONCAT(mobile,last_name)'), $record->uhash2)->first()->pluck('uuid');
        }

        if (!count($rst)) {
            $rst = Consumer::select('mobile', 'id')->where('mobile', $record->consumer_id)->first()->pluck('uuid');
        }

        if (count($rst)) {
            var_dump($rst);
        }

        var_dump($record->id);*/

        echo "Processing record {$record->id} with {$record->uhash1}/{$record->uhash2} and {$record->consumer_id} \n";

        $rst = Consumer::select(DB::raw('CONCAT(mobile,first_name,last_name) AS uhash1'), 'id', 'uuid')->where(DB::raw('CONCAT(mobile,first_name,last_name)'), $record->uhash1)->first();

        if (count($rst) > 0) {
            return $this->createInteraction($rst, $record);
        } else {

            $rst = Consumer::select(DB::raw('CONCAT(mobile,last_name) AS uhash2'), 'id', 'uuid')->where(DB::raw('CONCAT(mobile,last_name)'), $record->uhash2)->first();

            if (count($rst) > 0) {
                return $this->createInteraction($rst, $record);
            } else {
                $rst = Consumer::select('mobile', 'id', 'uuid')->where('mobile', $record->consumer_id)->first();

                if (count($rst) > 0) return $this->createInteraction($rst, $record);
            }
        }

        $r = RawDmSelection::find($record->id);
        $r->processed = 1;
        $r->save();
    }

    protected function createInteraction($consumerRecord, $record)
    {
        $unix_date = ($record->timestamp - 25569) * 86400;
        $timestamp = gmdate("Y-m-d H:i:s", $unix_date);

        $int = Interaction::where('uuid', '=', $consumerRecord->uuid)->orderBy('timestamp', 'DESC')->first();

        $int->purge('id');
        $int->interaction_type = 'DM_SELECTION';
        $int->campaign_code = $record->campaign_id;
        $int->timestamp = $timestamp;
        $int->timestamp_unix = $unix_date;
        $int->exists = false;

        $r = RawDmSelection::find($record->id);

        if ($int->save()) {
            $r->processed = 2;
        } else {
            $r->processed = 4;
        }

        $r->save();
    }
}