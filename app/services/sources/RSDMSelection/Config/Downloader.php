<?php

namespace Services\Sources\RSDMSelection\Config;

use Services\Sources\Base\BaseDownloader;
use Illuminate\Support\Collection;
use PHPExcel_IOFactory;
use RawDmSelection;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\DB;

class Downloader extends BaseDownloader  {

    protected $type;

    public function download() {

        $finder = new Finder();
        $files = $finder->files()->in('./app/external/selections/files');

        foreach ($files as $file)
        {

            $fileToParse = './app/external/selections/temp/' . strtolower($file->getFilename());
            $lowerContent = new Process("awk '{ print tolower($0)}' {$file->getRealpath()} > {$fileToParse}");
            $lowerContent->mustRun();

            $query = sprintf("LOAD DATA LOCAL INFILE '%s' INTO TABLE crm_dev.raw_data_dm_selection FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' ESCAPED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 LINES", addslashes($fileToParse));

            DB::connection()->getpdo()->exec($query);

            $removeTemp = new Process("rm -f {$fileToParse}");
            $removeTemp->mustRun();

        }
    }

}