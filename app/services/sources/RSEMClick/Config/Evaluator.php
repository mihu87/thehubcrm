<?php

namespace Services\Sources\RSEMClick\Config;

use Services\Sources\Base\BaseEvaluator;

class Evaluator extends BaseEvaluator  {

    protected $unicityFields = ['first_name', 'last_name', 'birth_date', 'phone'];

    protected $junkRules = [

    ];
}