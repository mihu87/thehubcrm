<?php

namespace Services\Sources;
use Illuminate\Support\Facades\Queue;

/**
 * Class Source
 * @package Services\Sources
 * @method Source getDownloader()
 * @method Source getTranslator()
 * @method Source getInjector()
 * @method Source getJunker()
 * @method Source getNormalizer()
 * @method Source getValidator()
 * @method Source getEvaluator()
 */
abstract class Source {

    const SOURCE_CONFIG_FOLDER_NAME = 'Config';

    /**
     * @var array
     */
    protected $configs = [
        'Downloader',
        'Translator',
        'Normalizer',
        'Injector',
        'Junker',
        'Validator',
        'Evaluator'
    ];


    public function __construct() {
        foreach ($this->getConfigClasses() as $property => $object) {
            if (class_exists($object)) {
                $this->{$property} = \App::make($object);
            }
        }
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws \Exception
     */
    public function __call($name, $arguments) {

        $configurator = ucfirst(str_replace('get', '', $name));

        if (in_array($configurator, $this->configs)) {
            return $this->{lcfirst($configurator)};
        } else {
            throw new \Exception('Config not found for : ' . $configurator);
        }
    }

    public function download() {

        /** @var \Services\Sources\Base\BaseDownloader $downloader */
        $downloader = $this->getDownloader();
        $downloader->download();

        //$downloader = $this->getDownloader();
        //$data = $downloader->getData();

        //return $data;
    }


    public function process($record) {

        /** @var \Services\Sources\Base\BaseTranslator $translator */

        $translator = $this->getTranslator();
        $translatedRecord = $translator->getTranslatedRecord($record);

        /** @var \Services\Sources\Base\BaseNormalizer $normalizer */

        $normalizer = $this->getNormalizer();
        $normalizedRecord = $normalizer->normalize($translatedRecord);

        /** @var \Services\Sources\Base\BaseValidator $validator */

        $validator = $this->getValidator();
        $validatedRecord = $validator->validate($normalizedRecord);

        /** @var \Services\Sources\Base\BaseEvaluator $evaluator */

        $evaluator = $this->getEvaluator();
        $evaluatedRecord = $evaluator->evaluate($validatedRecord);

        /** @var \Services\Sources\Base\BaseInjector $injector */
        $injector = $this->getInjector();

        /** @var \Services\Sources\Base\BaseJunker $junker */
        $junker = $this->getJunker();

        $add = [false, false];

        if ($evaluatedRecord->chk->junk) {
            //$add = $junker->push($evaluatedRecord);
        } else {
            $add = $injector->push($evaluatedRecord);
        }

        return $add;
    }

    /**
     * @return string
     */
    protected function getConfigNameSpace() {

        $calledClass = get_called_class();
        $calledClassName = explode("\\", $calledClass);
        $calledClassName = $calledClassName[count($calledClassName)-1];
        $calledNamespace = preg_replace('/\b' . $calledClassName . '\b/', '', $calledClass);

        return $calledNamespace . self::SOURCE_CONFIG_FOLDER_NAME . "\\";
    }

    /**
     * @return array
     */
    protected function getConfigClasses() {

        $configs = [];

        foreach ( $this->configs as $configClass ) {
            $configs[camel_case($configClass)] = $this->getConfigNameSpace() . $configClass;

        }

        return $configs;
    }

    public function getType() {
        return $this->type;
    }

}

/**
 * SELECT NULL as id, LEFT(UUID(), 20) AS external_id, SHA1( CONCAT( LOWER( fname ) , LOWER( lname ) , LOWER( birthdate ) , LOWER(mobile ) ) ) AS uuid, LOWER( fname ) AS first_name, LOWER( lname ) AS last_name, LOWER( birthdate ) AS birth_date, NULL AS gender, NULL AS cnp, LOWER( mobile ) AS mobile, LOWER( email ) AS email, LOWER( county ) AS county, LOWER( city ) AS city, NULL AS street_type, LOWER( street ) AS street_name, LOWER( no ) AS street_no,LOWER( building ) AS building, LOWER( entrance ) AS entrance, LOWER( floor ) AS floor, LOWER( apartment ) ASapartment, 0 AS zip_code, LOWER( optin ) AS  optin_optout, LOWER( sku_name ) AS brand_regular, LOWER( sku_code ) AS brand_regular_sku, LOWER( first_seen ) AS first_seen, LOWER( last_seen ) AS last_seen, 0 AS valid_phone, 0 ASvalid_email, 0 AS valid_dm, NULL AS created_at, NULL AS updated_at FROM crmdb.init_consumers INTO OUTFILE 'copy.csv';

 */
