<?php

namespace Services\Sources\RSSMSSelection;

use Services\Sources\Source;
use Illuminate\Support\Facades\DB;
use Interaction;
use Carbon\Carbon;

class SMSSelection extends Source {

    public $type = 'selection';
    public $rawTable = 'raw_data_sms_selection';

    public function processIn($record) {

        $cData  = DB::table('consumers')->where('mobile', '=', $record->participant)->first();

        if (count($cData)) {

            $curDate = date_parse_from_format('m-d-y', $record->timestamp);
            $carbon = new Carbon();
            $carbon->year($curDate['year']);
            $carbon->month($curDate['month']);
            $carbon->day($curDate['day']);

            $int = Interaction::where('uuid', '=', $cData->uuid)->orderBy('timestamp', 'DESC')->first();
            $int->purge('id');
            $int->interaction_type = 'SMS_SELECTION';
            $int->campaign_code = $record->campaign_id;
            $int->timestamp = $carbon->endOfDay()->format('Y-m-d H:i:s');
            $int->timestamp_unix = $carbon->endOfDay()->getTimestamp();
            $int->exists = false;
            $int->save();

        }
    }
}