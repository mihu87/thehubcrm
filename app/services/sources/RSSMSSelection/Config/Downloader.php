<?php

namespace Services\Sources\RSSMSSelection\Config;

use Services\Sources\Base\BaseDownloader;
use Illuminate\Support\Collection;

use PHPExcel_IOFactory;
use RawSmsSelection;

class Downloader extends BaseDownloader  {

    protected $type;

    public function download() {

        $file = './app/external/in/VF30_SMS selectie.xlsx';
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($file);

        $lines =  $objPHPExcel->getActiveSheet()->toArray();
        $toAdd = [];

        for ($i=1; $i<count($lines); ++$i) {
            if(array_filter($lines[$i])) {
                $toAdd[] = array_combine($lines[0], $lines[$i]);
            }
        }

        RawSmsSelection::insert($toAdd);
    }
}