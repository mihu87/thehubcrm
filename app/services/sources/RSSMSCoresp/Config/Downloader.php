<?php

namespace Services\Sources\RSSMSCoresp\Config;

use Services\Sources\Base\BaseDownloader;
use Illuminate\Support\Collection;
use PHPExcel_IOFactory;
use RawSmsCoresp;

class Downloader extends BaseDownloader  {

    protected $type;

    public function download() {

        $file = './app/external/in/198_2_SMS coresp.xlsx';

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($file);

        $lines =  $objPHPExcel->getActiveSheet()->toArray();
        $toAdd = [];

        for ($i=1; $i<count($lines); ++$i) {
            $toAdd[] = array_combine($lines[0], $lines[$i]);
        }

        RawSmsCoresp::insert($toAdd);

    }
}