<?php

namespace Services\Sources\RSSMSCoresp;

use Services\Sources\Source;
use Interaction;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SMSCoresp extends Source {

    public $type = 'selection';
    public $rawTable = 'raw_data_sms_coresp';

    public function processIn($record) {

        $cData  = DB::table('consumers')->where('mobile', '=', $record->SMS)->first();

        if (count($cData)) {

            $curDate = date_parse_from_format('m-d-y', $record->timestamp);
            $carbon = new Carbon();
            $carbon->year($curDate['year']);
            $carbon->month($curDate['month']);
            $carbon->day($curDate['day']);


            $int = Interaction::where('uuid', '=', $cData->uuid)->orderBy('timestamp', 'DESC')->first();
            $int->purge('id');
            $int->interaction_type = 'SMS_CORESP';
            $int->claim_code = $record->cod;
            $int->timestamp = $carbon->endOfDay()->format('Y-m-d H:i:s');
            $int->timestamp_unix = $carbon->endOfDay()->getTimestamp();
            $int->exists = false;
            $int->save();

        }

    }

}