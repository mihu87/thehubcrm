<?php

namespace Services\Sources\Base;

use Illuminate\Support\Facades\DB;
use Interaction;
use Consumer;
use ConsumerJunk;

use PDO;

abstract class BaseEvaluator {

    protected $dbh;

    public function __construct()
    {
        $this->dbh = new PDO("mysql:host=localhost;dbname=" . \Config::get('database.connections.mysql.database'),"root","123root456");
        $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function evaluate(&$record) {

        if ($this->isJunk($record)) {
            $record->chk->junk = true;
        }

        $current = false;

        if ($record->chk->junk) {
            $current = $this->junkConsumerExists($record);
        } else {
            $current = $this->consumerExists($record);
        }

        if (!$current) {
            $record->chk->new = true;
        } else {
            $record->chk->id = $current;
        }

        return $record;
    }

    protected function consumerExists($record) {

        $hash = $record->first_name . $record->last_name . $record->birth_date . $record->mobile;
        $sql = 'SELECT id from consumers WHERE uuid = ?';
        $stmt = $this->dbh->prepare($sql);
        $stmt->bindParam(1, $hash, PDO::PARAM_STR);
        $stmt->execute();

        if($stmt->rowCount() == 0) return false;

        return $stmt->fetchColumn(0);

    }

    protected function junkConsumerExists($record) {

        $hash = $record->first_name . $record->last_name . $record->birth_date . $record->mobile;
        $hash = $record->first_name . $record->last_name . $record->birth_date . $record->mobile;
        $sql = 'SELECT id from consumers_junk WHERE uuid = ?';
        $stmt = $this->dbh->prepare($sql);
        $stmt->bindParam(1, $hash, PDO::PARAM_STR);
        $stmt->execute();

        if($stmt->rowCount() == 0) return false;

        return $stmt->fetchColumn(0);
    }

    protected function isJunk($record) {

        $exclude = [];

        foreach ($this->junkRules as $rule) {
            $valid = call_user_func_array($rule, [$record]);
            if (!$valid) $exclude[] = $valid;
        }

        return count($exclude) > 0 ? true : false;
    }

}