<?php
/**
 * @package Services
 */
namespace Services\Sources\Base;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use JMS\Serializer\SerializerBuilder;


/**
 * Class BaseDownloader
 * @package Services\Sources\Base
 */
abstract class BaseDownloader {

    /** @var \Symfony\Component\Console\Output\ConsoleOutput  */
    protected $console;

    /** @var \JMS\Serializer\Serializer */
    protected $serializer;

    /** @var \Symfony\Component\Serializer\Encoder\JsonEncoder  */
    protected $encoder;

    /** @var string */
    protected $queueName;

    /** @var string */
    protected $source;

    /**
     * @param ConsoleOutput $consoleOutput
     * @param JsonEncoder $encoder
     */
    public function __construct(ConsoleOutput $consoleOutput, JsonEncoder $encoder) {

        \Doctrine\Common\Annotations\AnnotationRegistry::registerAutoloadNamespace(
            'JMS\Serializer\Annotation',
            base_path() . "/vendor/jms/serializer/src");

        $this->console = $consoleOutput;
        $this->serializer = SerializerBuilder::create(true)->build();
        $this->encoder = $encoder;

    }

    public function publish($message) {

    }

    abstract public function download();
}