<?php

namespace Services\Sources\Base;

use Illuminate\Support\Facades\DB;

use Interaction;
use Consumer;

use PDO;

abstract class BaseInjector
{
    const SCENARIO_UPDATE = 'UPDATE';
    const SCENARIO_INSERT = 'INSERT';

    protected $dbh;

    public function __construct()
    {
        $this->dbh = new PDO("mysql:host=localhost;dbname=" . \Config::get('database.connections.mysql.database'),"root","123root456");
        $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function push($record) {

         if ($record->chk->new) {
            return $this->add($record);
        } else {
            return $this->update($record);
        }

        throw new \RuntimeException('Maybe there was a problem when deciding to junk/add/update the record.');
    }

    protected function update($record) {

        $diffs = [];
        $skip   = ['id', 'uuid'];

        $checks = [
            'email_change' => ['email'],
            'brand_change' => ['brand_regular'],
            'address_change' => [
                'county',
                'city',
                'street_name',
                'street_no',
                'building',
                'entrance',
                'floor',
                'apartment'
            ]
        ];


        //int

        $interaction = [];

        foreach ($record as $k => $v) {
            if (!is_object($v)) $interaction[$k] = $v;
        }

        $interaction['interaction_type'] = 'interaction';
        $interaction['uuid'] = $record->first_name.$record->last_name.$record->birth_date.$record->mobile;

        $columnString = implode(',', array_keys($interaction));
        $valueString = implode(',', array_fill(0, count($interaction), '?'));

        $stmt = $this->dbh->prepare("INSERT INTO interactions ({$columnString}) VALUES ({$valueString})");
        $intEx = $stmt->execute(array_values($interaction));

        $intId = $this->dbh->lastInsertId();

        $stmt = $this->dbh->prepare('SELECT * FROM consumers WHERE id = :id');
        $stmt->execute([':id' => $record->chk->id]);

        $cs = $stmt->fetchAll(PDO::FETCH_ASSOC)[0];

        //var_dump($record->chk->id);

        foreach ($cs as $k => $v) {

            if (in_array($k, array_flatten($checks))) {

                if ($record->{$k} != $v && !empty($record->{$k})) {

                    $hasIndex = $this->array_search_recursive($k, $checks, $idx);
                    if ($hasIndex) {
                        $diffs[$idx[0]] = [$k => $record->{$k}];
                    }

                }
            }
        }

        if (count($diffs)) {

            foreach ($diffs as $changeType => $changeSet) {

                foreach ($changeSet  as $field => $value) {

                    $interaction['interaction_type'] = $changeType;
                    $interaction['timestamp'] = $record->timestamp;

                    $columnString = implode(',', array_keys($interaction));
                    $valueString = implode(',', array_fill(0, count($interaction), '?'));

                    $stmt = $this->dbh->prepare("INSERT INTO interactions ({$columnString}) VALUES ({$valueString})");
                    $intEx = $stmt->execute(array_values($interaction));
                }

            }
        }

        // update Last SEEN

        return [$record->chk->id, self::SCENARIO_UPDATE];

    }

    protected function add($record) {

        $consumer = [];
        $consumer['uuid'] = $record->first_name.$record->last_name.$record->birth_date.$record->mobile;
        $consumer['external_id'] = $record->external_id;
        $consumer['first_name'] = $record->first_name;
        $consumer['last_name'] = $record->last_name;
        $consumer['birth_date'] = $record->birth_date;
        $consumer['gender'] = $record->gender;
        $consumer['cnp'] = $record->cnp;
        $consumer['mobile'] = $record->mobile;
        $consumer['email'] = $record->email;
        $consumer['county'] = $record->county;
        $consumer['city'] = $record->city;
        $consumer['street_type'] = $record->street_type;
        $consumer['street_name'] = $record->street_name;
        $consumer['street_no'] = $record->street_no;
        $consumer['building'] = $record->building;
        $consumer['entrance'] = $record->entrance;
        $consumer['floor'] = $record->floor;
        $consumer['apartment'] = $record->apartment;
        $consumer['zip_code'] = $record->zip_code;
        $consumer['optin_optout'] = $record->optin_optout;
        $consumer['brand_regular'] = $record->brand_regular;
        $consumer['brand_regular_sku'] = $record->brand_regular_sku;
        $consumer['first_seen'] = $record->timestamp;
        $consumer['last_seen'] = $record->timestamp;
        $consumer['created_at'] = 'NOW()';

        $csId = null;

        try
        {
            $columnString = implode(',', array_keys($consumer));
            $valueString = implode(',', array_fill(0, count($consumer), '?'));

            $stmt = $this->dbh->prepare("INSERT INTO consumers ({$columnString}) VALUES ({$valueString})");
            $ex = $stmt->execute(array_values($consumer));

            $csId = $this->dbh->lastInsertId();

            if ($ex) {

                unset($consumer['first_seen'], $consumer['last_seen']);

                $interaction = [];

                foreach ($record as $k => $v) {
                    if (!is_object($v)) $interaction[$k] = $v;
                }

                $interaction['uuid'] = $consumer['uuid'];

                $ints = [
                    array_merge($interaction, ['interaction_type' => 'new']),
                    array_merge($interaction, ['interaction_type' => 'interaction'])
                ];

                foreach ($ints as $val) {

                    $columnString = implode(',', array_keys($val));
                    $valueString = implode(',', array_fill(0, count($val), '?'));

                    $stmt = $this->dbh->prepare("INSERT INTO interactions ({$columnString}) VALUES ({$valueString})");
                    $intEx = $stmt->execute(array_values($val));
                }


            }

            // Update LAST Seen

        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return [$csId, self::SCENARIO_INSERT];
    }

    public function array_search_recursive($needle, $haystack, &$indexes=array())
    {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $indexes[] = $key;
                $status = $this->array_search_recursive($needle, $value, $indexes);
                if ($status) {
                    return true;
                } else {
                    $indexes = array();
                }
            } else if ($value == $needle) {
                $indexes[] = $key;
                return true;
            }
        }
        return false;
    }
}