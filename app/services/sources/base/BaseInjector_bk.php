<?php

namespace Services\Sources\Base;

use Illuminate\Support\Facades\DB;

use Interaction;
use Consumer;

use PDO;

abstract class BaseInjector {

    const SCENARIO_UPDATE = 'UPDATE';
    const SCENARIO_INSERT = 'INSERT';

    public function push($record) {

        if ($record->chk->new) {
            return $this->add($record);
        } else {
            return $this->update($record);
        }

        throw new \RuntimeException('Maybe there was a problem when deciding to junk/add/update the record.');
    }

    protected function update($record) {

        $diffs = [];
        $skip   = ['id', 'uuid'];

        $checks = [
            'email_change' => ['email'],
            'brand_change' => ['brand_regular'],
            'address_change' => [
                'county',
                'city',
                'street_name',
                'street_no',
                'building',
                'entrance',
                'floor',
                'apartment'
            ]
        ];


        /*
        $consumer = Consumer::find($record->chk->id);

        unset($record->chk);

        foreach ($consumer->toArray() as $k => $v) {

            if (in_array($k, array_flatten($checks))) {

                if ($record->{$k} != $v && !empty($record->{$k})) {

                    $hasIndex = $this->array_search_recursive($k, $checks, $idx);
                    if ($hasIndex) {
                        $diffs[$idx[0]] = [$k => $record->{$k}];
                    }

                }
            }
        }
        */
        //$this->pdo = new PDO("mysql:host=localhost;dbname=crm_dev3","root","123root456");


        /** Create interaction */


        $i = new Interaction;
        foreach ($record as $k => $v) {
            if (!in_array($k, $skip)) {
                $i->{$k} = $v;
                $i->interaction_type = 'interaction';
            }
        }
        $i->save();

        if (count($diffs)) {

            foreach ($diffs as $changeType => $changeSet) {

                foreach ($changeSet  as $field => $value) {

                    $obj = Interaction::find($i->id);
                    $obj->purge('id');
                    $obj->{$field} = $value;
                    $obj->uuid = $i->uuid;
                    $obj->interaction_type = $changeType;
                    $obj->timestamp = $i->timestamp;
                    $obj->birth_date = $i->birth_date;
                    $obj->exists = false;
                    $obj->save();

                }
            }

        }

        return [$record->external_id, self::SCENARIO_UPDATE];
    }

    protected function add($record) {

        unset($record->chk);

        $consumer = new Consumer;

        $consumer->uuid = $record->first_name.$record->last_name.$record->birth_date.$record->mobile;
        $consumer->external_id = $record->external_id;
        $consumer->first_name = $record->first_name;
        $consumer->last_name = $record->last_name;
        $consumer->birth_date = $record->birth_date;
        $consumer->gender = $record->gender;
        $consumer->cnp = $record->cnp;
        $consumer->mobile = $record->mobile;
        $consumer->email = $record->email;
        $consumer->county = $record->county;
        $consumer->city = $record->city;
        $consumer->street_type = $record->street_type;
        $consumer->street_name = $record->street_name;
        $consumer->street_no = $record->street_no;
        $consumer->building = $record->building;
        $consumer->entrance = $record->entrance;
        $consumer->floor = $record->floor;
        $consumer->apartment = $record->apartment;
        $consumer->zip_code = $record->zip_code;
        $consumer->optin_optout = $record->optin_optout;
        $consumer->brand_regular = $record->brand_regular;
        $consumer->brand_regular_sku = $record->brand_regular_sku;
        $consumer->first_seen = $record->timestamp;
        $consumer->last_seen = $record->timestamp;

        $consumer->save();

        $interaction = new Interaction;

        foreach ($record as $property => $value) {
            $interaction->{$property} = $value;
        }

        $interaction->interaction_type = 'new_consumer';

        if ($interaction->save()) {

            $interaction->purge('id');
            $interaction->interaction_type = 'interaction';
            $interaction->exists = false;
            $interaction->save();

            if ($interaction->save()) {
                return [$interaction->id, self::SCENARIO_INSERT];
            }

        }

    }

    /*protected function junk($record) {

        unset($record->chk);

        $junkInteraction = new InteractionJunk;

        foreach ($record as $k => $v) {
            $junkInteraction->{$k} = $v;
        }

        if ( $junkInteraction->save() ) {
            return [$junkInteraction->id, self::SCENARIO_JUNK];
        }

        throw new \RuntimeException('Cannot save the junk interaction due to an unknown error.');
    }*/

    public function array_search_recursive($needle, $haystack, &$indexes=array())
    {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $indexes[] = $key;
                $status = $this->array_search_recursive($needle, $value, $indexes);
                if ($status) {
                    return true;
                } else {
                    $indexes = array();
                }
            } else if ($value == $needle) {
                $indexes[] = $key;
                return true;
            }
        }
        return false;
    }
}