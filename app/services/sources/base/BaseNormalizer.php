<?php

namespace Services\Sources\Base;

abstract class BaseNormalizer {

    const ADDITIOAL_FIELDS_PREFIX = 'chk';
    protected $normalizationRules;
    protected $additionalFields;

    /**
     * @param $record
     */
    public function normalize(&$record) {

        foreach ($this->normalizationRules as $fieldKey => $function) {
            if (is_int($fieldKey)) {
                foreach ($this->getVars($record) as $key => $var) {
                        $record->{$key} = call_user_func_array($function, [$record->{$key}]);
                }
            } else {
                if (isset($record->{$fieldKey})) {
                    if (is_array($function)) {
                        foreach ($function as $fn) {
                            $record->{$fieldKey} = call_user_func_array($fn, [$record->{$fieldKey}]);
                        }
                    } else {
                        $record->{$fieldKey} = call_user_func_array($function, [$record->{$fieldKey}]);
                    }
                }
            }
        }

        $record = $this->additionalFields($record);

        return $record;
    }

    protected function additionalFields(&$record) {

        if (is_array($this->additionalFields) && !empty($this->additionalFields)) {

            $additional = new \stdClass();

            foreach ($this->additionalFields as $property => $value) {
                if (is_int($property)) {
                    $additional->{$value} = -1;
                } else {
                    $additional->{$property} = $value;
                }
            }

            $prefix = self::ADDITIOAL_FIELDS_PREFIX;
            $record->{$prefix} = $additional;
        }

        return $record;
    }

    /**
     * @param $record
     * @return array
     */
    protected function getVars($record) {

        if (method_exists($record, 'getAttributes')) {
            return $record->getAttributes();
        } else {
            return get_object_vars($record);
        }
    }
}