<?php

namespace Services\Sources\Base;

use Interaction;
use Symfony\Component\PropertyAccess\PropertyAccessor;

abstract class BaseTranslator {

    protected $neutral = [
        'id',
        'uuid',
        'created_at',
        'updated_at',
    ];

    public function getTranslationMap() {

        $interaction = new Interaction();

        $fields = array_diff($interaction->getColumnNames(), $this->neutral);

        $map = [];

        foreach ($fields as $modelField) {
            $corespondence = array_search($modelField, $this->translationMap);
            if ($corespondence != false) {
                $map[$corespondence] = $modelField;
            } else {
                $map[] = $modelField;
            }
        }

        return $map;
    }


    public function getTranslatedRecord($record) {

        $accessor = new PropertyAccessor();

        $record = ( !is_object($record) ? (object) $record : $record );
        $obj = new \stdClass();

        foreach ($this->getTranslationMap() as $origin => $destination) {

            if ( is_int($origin) ) {
                $obj->{$destination} = null;
            } else {
                $obj->{$destination} = isset($record->{$origin}) ? $accessor->getValue($record, $origin) : null;
            }
        }

        unset($record);

        return $obj;
    }
}