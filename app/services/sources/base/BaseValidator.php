<?php

namespace Services\Sources\Base;

abstract class BaseValidator {

    /**
     * @param $record
     */
    public function validate(&$record) {

        $invalidFields = [];

        foreach ($this->validationRules as $fieldKey => $function) {

            if (is_int($fieldKey)) {
                foreach ($this->getVars($record) as $key => $var) {
                    $isValid = call_user_func_array($function, [$record->{$key}]);
                    if (!$isValid) $invalidFields[$key] = (int) $isValid;
                }

            } else {

                if (isset($record->{$fieldKey})) {
                    if (is_array($function)) {
                        foreach ($function as $fn) {
                            $isValid = call_user_func_array($fn, [$record->{$fieldKey}]);
                            if (!$isValid) $invalidFields[$fieldKey] = (int) $isValid;
                        }
                    } else {
                        $isValid = call_user_func_array($function, [$record->{$fieldKey}]);
                        if (!$isValid) $invalidFields[$fieldKey] = (int) $isValid;
                    };
                }
            }
        }

        if (count($invalidFields)) {
            $record->chk->valid = false;
            $record->chk->invalid_fields = (object) $invalidFields;
        }

        return $record;
    }



    /**
     * @param $record
     * @return array
     */
    protected function getVars($record) {

        if (method_exists($record, 'getAttributes')) {
            return $record->getAttributes();
        } else {
            return get_object_vars($record);
        }
    }
}