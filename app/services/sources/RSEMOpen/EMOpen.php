<?php

namespace Services\Sources\RSEMOpen;

use Services\Sources\Source;
use Illuminate\Support\Facades\DB;
use Interaction;
use Carbon\Carbon;

class EMOpen extends Source {

    public $type = 'selection';
    public $rawTable = 'raw_data_em_open';

    public function processIn($record) {

        $cData  = DB::table('consumers')->where('email', '=', $record->email)->first();

        if (count($cData)) {

            $carbon = new Carbon($record->timestamp);

            $int = Interaction::where('uuid', '=', $cData->uuid)->orderBy('timestamp', 'DESC')->first();
            $int->purge('id');
            $int->interaction_type = 'EMAIL_OPEN';
            $int->campaign_code = $record->campaign_id;
            $int->timestamp = $carbon->endOfDay()->format('Y-m-d H:i:s');
            $int->timestamp_unix = $carbon->endOfDay()->getTimestamp();
            $int->exists = false;
            $int->save();

        }
    }
}