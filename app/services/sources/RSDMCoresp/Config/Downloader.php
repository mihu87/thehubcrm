<?php

namespace Services\Sources\RSDMCoresp\Config;

use Services\Sources\Base\BaseDownloader;

use Illuminate\Support\Collection;
use PHPExcel_IOFactory;
use RawDmCoresp;

class Downloader extends BaseDownloader  {

    protected $type;

    public function download() {

        $file = './app/external/in/TH40_2_DM corespondenta.xlsx';

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($file);

        $lines =  $objPHPExcel->getActiveSheet()->toArray();

        $toAdd = [];

        for ($i=1; $i<count($lines); ++$i) {
            $toAdd[] = array_combine($lines[0], $lines[$i]);
        }

        foreach (array_chunk($toAdd, 1000) as $idListBatch) {
            RawDmCoresp::insert($idListBatch);
        }


    }
}