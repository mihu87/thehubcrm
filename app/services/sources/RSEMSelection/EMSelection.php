<?php

namespace Services\Sources\RSEMSelection;

use Services\Sources\Source;
use Interaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EMSelection extends Source {

    public $type = 'selection';
    public $rawTable = 'raw_data_em_selections';

    public function processIn($record) {

        $cData  = DB::table('consumers')->where('email', '=', $record->email)->first();

        if (count($cData)) {

            $curDate = date_parse_from_format('m-d-y', $record->timestamp);
            $carbon = new Carbon();
            $carbon->year($curDate['year']);
            $carbon->month($curDate['month']);
            $carbon->day($curDate['day']);

            $int = Interaction::where('uuid', '=', $cData->uuid)->orderBy('timestamp', 'DESC')->first();
            $int->purge('id');
            $int->interaction_type = 'EMAIL_SELECTION';
            $int->campaign_code = $record->campaign_id;
            $int->timestamp = $carbon->endOfDay()->format('Y-m-d H:i:s');
            $int->timestamp_unix = $carbon->endOfDay()->getTimestamp();
            $int->exists = false;
            $int->save();

        }
    }

}