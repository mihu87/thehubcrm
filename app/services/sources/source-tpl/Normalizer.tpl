<?php

namespace Services\Sources\#name#\Config;

use Services\Sources\Base\BaseNormalizer;

class Normalizer extends BaseNormalizer {

    protected $strict = false;

    protected $normalizationRules = [
        //rules here
    ];


    protected $validationRules = [
        //rules here
    ];

}