<?php

namespace Services\Sources\RSEMCoresp\Config;

use Services\Sources\Base\BaseNormalizer;

class Normalizer extends BaseNormalizer {

    protected $strict = false;

    protected $normalizationRules = [
        //rules here
    ];


    protected $validationRules = [
        //rules here
    ];

}