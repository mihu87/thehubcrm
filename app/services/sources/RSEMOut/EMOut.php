<?php

namespace Services\Sources\RSEMOut;

use Services\Sources\Source;
use Carbon\Carbon;
use Interaction;
use Consumer;
use Illuminate\Support\Facades\DB;

class EMOut extends Source {

    public $type = 'selection';
    public $rawTable = 'raw_data_em_out';

    public function processIn($record) {

        $cData  = DB::table('consumers')->where('email', '=', $record->email)->first();

        if (count($cData)) {

            $curDate = date_parse_from_format('m-d-y', $record->timestamp);
            $carbon = new Carbon();
            $carbon->year($curDate['year']);
            $carbon->month($curDate['month']);
            $carbon->day($curDate['day']);

            $int = Interaction::where('uuid', '=', $cData->uuid)->orderBy('timestamp', 'DESC')->first();
            $int->purge('id');
            $int->interaction_type = 'EMAIL_OUT';
            $int->campaign_code = $record->campaign_id;
            $int->email_bounce = 'hb';
            $int->timestamp = $carbon->endOfDay()->format('Y-m-d H:i:s');
            $int->timestamp_unix = $carbon->endOfDay()->getTimestamp();
            $int->exists = false;
            $int->save();

            $consumer = Consumer::find($cData->id);
            $consumer->email_status = 'hb';
            $consumer->save();

        }
    }

}