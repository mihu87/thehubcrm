<?php

namespace Services\Sources\RSEMOut\Config;

use Services\Sources\Base\BaseDownloader;
use Illuminate\Support\Collection;
use PHPExcel_IOFactory;
use RawEmOut;

class Downloader extends BaseDownloader  {

    protected $type;

    public function download() {

        $file = './app/external/in/TH40_2_e-mail out.xlsx';

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($file);

        $lines =  $objPHPExcel->getActiveSheet()->toArray();
        $toAdd = [];

        $headers = $lines[0];

        array_walk($headers, function (&$v, $k) { $v = str_replace('-', '', $v); });

        for ($i=1; $i<count($lines); ++$i) {
            $toAdd[] = array_combine($headers, $lines[$i]);
        }

        RawEmOut::insert($toAdd);

    }
}