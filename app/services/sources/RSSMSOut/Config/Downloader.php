<?php

namespace Services\Sources\RSSMSOut\Config;

use Services\Sources\Base\BaseDownloader;
use Illuminate\Support\Collection;
use PHPExcel_IOFactory;
use RawSmsOut;

class Downloader extends BaseDownloader  {

    protected $type;

    public function download() {

        $file = './app/external/in/TH40_SMS OUT.xlsx';

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($file);

        $lines =  $objPHPExcel->getActiveSheet()->toArray();
        $toAdd = [];

        for ($i=1; $i<count($lines); ++$i) {
           $toAdd[] = array_combine($lines[0], $lines[$i]);
        }

         RawSmsOut::insert($toAdd);

    }
}