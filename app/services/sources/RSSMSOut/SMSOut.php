<?php

namespace Services\Sources\RSSMSOut;

use Services\Sources\Source;
use Illuminate\Support\Facades\DB;
use Consumer;
use Interaction;

class SMSOut extends Source {

    public $type = 'selection';
    public $rawTable = 'raw_data_smsout';

    public function processIn($record) {

        $cData  = DB::table('consumers')->where('mobile', '=', $record->consumer_id)->first();

        if (count($cData)) {

            $int = Interaction::where('uuid', '=', $cData->uuid)->orderBy('timestamp', 'DESC')->first();

            $int->purge('id');
            $int->interaction_type = 'SMS_OUT';
            $int->phone_status = 'hb';
            $int->campaign_code = $record->campaign_id;
            $int->exists = false;
            $int->save();

            $consumer = Consumer::find($cData->id);
            $consumer->phone_status = 'hb';
            $consumer->save();

        }
    }
}