<?php

namespace Services;

class DRDA {

    public function get($sourceName) {

        $sourceClass = '\\Services\\DRDA\\Sources\\' . $sourceName;

        if (class_exists($sourceClass)) {
            return new $sourceClass;
        }

        return false;
    }

}