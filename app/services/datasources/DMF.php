<?php


namespace Services\DRDA\Sources;

use \Illuminate\Database\Eloquent;

class DMF extends Source {

    protected function getFieldsNormalizationMapping() {
        /**
         * ["incoming_field_name" => "local_db_field_name"]
         */
        return [
            "id",
            "consumer_id",
            "date_created",
            "date_modified",
            "modified_history",
            "firstname" => "first_name",
            "lastname" => "last_name",
            "birthdate" => "birth_date",
            "sex" => "gender",
            "cnp" => "cnp",
            "mobile" => "phone",
            "email" => "email",
            "county" => "county",
            "city" => "city",
            "street_type" => "street_type",
            "street_name" => "street_name",
            "nr" => "street_no",
            "bl" => "building",
            "sc" => "entrance",
            "et" => "floor",
            "ap" => "apartment",
            "optin" => "optin_optout",
            "sku_brand",
            "sku_type",
            "inactive",
            "partial",
            "cms_data",
        ];
    }
    /**
     * @return array An array of records
     */
    protected function downloadRawData() {

        /** @var TODO $consumers should be an iterator instance **/
        $consumers = \RemoteDmfConsumers::all();
        return $consumers->toArray();

    }
}