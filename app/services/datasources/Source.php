<?php
namespace Services\DRDA\Sources;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

abstract class Source {

    protected $map;

    protected $primaryUpdateCheckField = 'phone';

    protected $secondUpdateCheckFields = ['first_name', 'last_name', 'birth_date'];
    protected $secondUpdateCheckAlgorithm = 'md5';

    abstract protected function getFieldsNormalizationMapping();
    abstract protected function downloadRawData();


    public function run() {

        set_time_limit(0);

        $records = $this->downloadRawData();

        $finalized = [];

        $i = 0;
        foreach ($records as $record) {

            $translated = $this->translateFieldsAndStructure($record);

            /** Step 2 Validare record */

            $validated = $this->validateTranslatedFields($translated);

            $updateResult = false;

            echo ".Checking Step one.\n";

            $checkStepOne = \DB::table('consumers')
                ->where($this->primaryUpdateCheckField, $validated[$this->primaryUpdateCheckField])
                ->get();

            if (empty($checkStepOne)) {

                echo "..Step 1 did not found any records, checking step 2.\n";

                $this->secondUpdateCheckFields[] = $this->primaryUpdateCheckField;

                $table = \DB::table('consumers');
                foreach ($this->secondUpdateCheckFields as $field) {
                    $table->where($field, $validated[$field]);
                }
                $checkStep2 = $table->get();

                if (empty($checkStep2)) {

                    echo "...Step 2 did not found any records, inserting record in consumers.\n";

                    $dbInsert = \Consumers::create($validated);

                    echo "....DONE\n";

                } else {

                    echo "...Step 2 found an existing record, trying update and history fill.\n";
                    $updateResult = $this->addToHistoryAndUpdate($validated, $checkStep2);
                }

            } else {
                echo "..Step 1 found an existing record, trying update and history fill.\n";
                $updateResult = $this->addToHistoryAndUpdate($validated, $checkStepOne);
            }

            if ($updateResult) {
                echo ".Update RESULT : {$updateResult} \n";
            }


            if ($i > 5) {
                break;
            }

            $i++;
        }

        unset($records);

        /**
         * We are now dowing the validation process for all fields, according to specs.
         */

        return true;
    }

    protected function translateFieldsAndStructure($record) {

        $normalized = [];

        foreach ($this->getFieldsNormalizationMapping() as $remoteFieldName => $fieldName) {

            if (array_key_exists($remoteFieldName, $record))
                $normalized[$fieldName] = $record[$remoteFieldName];

        }

        unset($record);

        return $normalized;
    }

    protected function validateTranslatedFields($record) {

        $fields = ['valid' => 1, 'valid_msg' => null];

        /** @var $validator  \Illuminate\Support\Facades\Validator */
        $validator = Validator::make($record, [

                'email'         => ['required', 'email'],
                'first_name'    => ['required'],
                'last_name'     => ['required'],
                'gender'        => ['required'],
                'cnp'            => ['cnp'],
                'phone'         => ['min:10', 'numeric'],

            ],
            ['cnp' => 'Invalid CNP according to Romanian STANDARDS.']
        );

        if ($validator->fails()) {
            $fields['valid'] = 0;
            $fields['valid_msg'] = implode("|" , array_keys($validator->messages()->toArray()));
        }

        return array_merge($record, $fields);
    }

    protected function addToHistoryAndUpdate($info, $current) {

        $table = \DB::table('consumers');

        foreach ($info as $k => $v) {
            if (!in_array($v, array('created_at', 'updated_at', 'id'))) {
                $table->where($k, $v);
            }
        }

        $result = $table->get();

        if (empty($result)) {

            $id = (int) end($current)->id;
            $history = (array) end($current);

            unset($current['created_at'], $current['updated_at'], $current['id']);

            $diffs = array_diff($info, $history);


            $histories = [];
            foreach ($diffs as $field => $val) {

                $history[$field] = $val;
                $history['id_consumer'] = $id;
                //$historyAdd = \ConsumersHistory::create($history);
                unset($history['id']);

                $histories[] = $history;
            }

             $historyAdd = \DB::table('consumers_history')->insert($histories);
             $updated = \DB::table('consumers')->where('id', $id)->update($info);

            return true;
        }

        return false;
    }

    public function __toString() {
        return __CLASS__;
    }
}