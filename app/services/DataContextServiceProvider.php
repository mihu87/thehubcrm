<?php

namespace Services;

class DataContextServiceProvider extends \Illuminate\Support\ServiceProvider {

    public function register() {
        $this->app->bind('datacontext', function(){
            return new \Services\DataContext;
        });
    }
}