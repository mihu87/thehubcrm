<?php

namespace CHelpers\Normalizers;

use Illuminate\Support\Facades\DB;

class String {

    static public function convertDoubleSpaceToOneSpace($string) {
        return preg_replace('/(?:\s\s+|\n|\t)/', ' ', $string);
    }

    static public function convertSingleSpaceToUnderscore($string) {
        return preg_replace("/[[:blank:]]/", "_", $string);
    }

    static public function replaceInteractionCode($interaction) {

        if (empty($interaction)) {
            return 'none';
        }

        $outputAction = DB::table('nomenclature_interaction_type')->where('code', '=', $interaction)->first();

        if (isset($outputAction->id)) {
            return $outputAction->id;
        }

        return 'none';
    }

    static public function convertFromUtf8ToAsciiTranslit($string) {
        return @iconv('UTF-8', 'ASCII//TRANSLIT', $string);
    }

    static public function convertBirthDateToTimestamp($string) {

        if (empty($string)) {
            return null;
        }

        $t = date_parse($string);
        $timestamp = mktime($t['hour'], $t['minute'], $t['second'], $t['month'], $t['day'], $t['year']);
        return date("Y-m-d", $timestamp);
    }

    static public function convertTimestampToUnixTimestamp($string) {
        return @strtotime ($string);
    }

    static public function convertTextToBoolean($string) {

        return (mb_strtoupper(trim($string)) === mb_strtoupper ("yes")) ? true : false;
    }
}