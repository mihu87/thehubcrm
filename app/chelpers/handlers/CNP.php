<?php

namespace CHelpers\Handlers;

class CNP {
    /*
     * @access: protected
     * @stores: the value received from constructor function
     * @type: string
     */
    protected $cnp;

    /*
     * @access: protected
     * @stores: the counties of Romania(2011)
     * @type: array
     */
    protected $cities = array(1=>"Alba",2=>"Arad",3=>"Arges",4=>"Bacau",5=>"Bihor",6=>"Bistrita-Nasaud",7=>"Botosani",8=>"Brasov",9=>"Braila",10=>"Buzau",11=>"Caras-Severin",12=>"Cluj",13=>"Constanta",14=>"Covasna",15=>"Dambovita",16=>"Dolj",17=>"Galati",18=>"Gorj",19=>"Harghita",20=>"Hunedoara",21=>"Ialomita",22=>"Iasi",23=>"Ilfov",24=>"Maramures",25=>"Mehedinti",26=>"Mures",27=>"Neamt",28=>"Olt",29=>"Prahova",30=>"Satu Mare",31=>"Salaj",32=>"Sibiu",33=>"Suceava",34=>"Teleorman",35=>"Timis",36=>"Tulcea",37=>"Vaslui",38=>"Valcea",39=>"Vrancea",41=>"Bucuresti/Sectorul 1",42=>"Bucuresti/Sectorul 2",43=>"Bucuresti/Sectorul 3",44=>"Bucuresti/Sectorul 4",45=>"Bucuresti/Sectorul 5",46=>"Bucuresti/Sectorul 6",51=>"Calarasi",52=>"Giurgiu");

    /*
     * @access: protected
     * @stores: the name of month in Romanian language
     * @type: array
     */
    protected $months = array(1=>"Ianuarie",2=>"Februarie",3=>"Martie",4=>"Aprilie",5=>"Mai",6=>"Iunie",7=>"Iulie",8=>"August",9=>"Septembrie",10=>"Octombrie",11=>"Noiembrie",12=>"Decembrie");

    /*
     * @access: public
     * @stores: the number of generated error:
     *        1 - if the length of CNP differs from 13,
     *        2 - if the CNP are invalid,
     *        3 - if sex, month, day or city are invalid
     * @type: integer
     */
    public $error;

    /*
     * @access: public
     * @param: string $cnp
     * @returns: false/void
     */
    public function __construct($cnp) {
        $key = "279146358279";
        if(strlen($cnp) !== 13) {
            $this->error = 1;
            return false;
        }
        else {
            $s = 0;
            for($i = 0; $i <= 11; $i++)
                $s += $cnp[$i] * $key[$i];
            $s %= 11;

            if(($s === 10 && $cnp[12] !== '1') || ($s < 10 && $s != $cnp[12])) {
                $this->error = 2;
                return false;
            }
            $this->cnp = $cnp;

            $this->genre = substr($this->cnp, 0, 1);
            $this->month = substr($this->cnp, 3, 2);
            $this->day = substr($this->cnp, 5, 2);
            $this->city = (int)substr($this->cnp, 7, 2);
            if(
                ($this->genre !== '1' && $this->genre !== '2' && $this->genre !== '5' && $this->genre !== '6') || // invalid genre
                (1 > $this->month || 12 < $this->month) || // invalid month
                (1 > $this->day || 31 < $this->day) || // invalid day
                (!isset($this->cities[$this->city])) // invalid county
            ) {
                $this->error = 3;
                return false;
            }
        }
    }

    /*
     * @access: public
     * @returns: array
     */
    public function fetch_all_data() {
        return array(
            'genre' => $this->get_genre(),
            'year' => $this->get_year(),
            'month' => $this->get_month(),
            'day' => $this->get_day(),
            'city' => $this->get_city()
        );
    }

    /*
     * @access: public
     * @returns: array
     */
    public function get_genre() {
        return array(
            $this->genre,
            $this->genre === '1' || $this->genre === '5' ? 'm' : 'f'
        );
    }

    /*
     * @access: public
     * @returns: string
     */
    public function get_year() {
        $year_prefix = '';
        $genre = $this->get_genre();

        if( $genre[0] == 1 || $genre[0] == 2 ) $year_prefix = 19;
        if( $genre[0] == 3 || $genre[0] == 4 ) $year_prefix = 18;
        if( $genre[0] == 5 || $genre[0] == 6 ) $year_prefix = 20;

        return $year_prefix . substr($this->cnp, 1, 2);
    }

    /*
     * @access: public
     * @returns: array
     */
    public function get_month() {
        return array(
            $this->month,
            $this->months[(int)$this->month]
        );
    }

    /*
     * @access: public
     * @returns: string
     */
    public function get_day() {
        return $this->day;
    }

    /*
     * @access: public
     * @returns: array
     */
    public function get_city() {
        return array (
            $this->city,
            $this->cities[(int)$this->city]
        );
    }
}