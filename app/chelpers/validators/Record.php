<?php

namespace CHelpers\Validators;

class Record {

    /*static public function hasAllDefinitionFieldsEmpty($record) {

        if (!isset($record->first_name) || empty($record->first_name) &&
            !isset($record->last_name) || empty($record->last_name)  &&
            !isset($record->birth_date) || empty($record->birth_date) &&
            !isset($record->mobile) || empty($record->mobile)) {

            return false;
        }
        return true;
    }*/

    static public function hasAllDefinitionFieldsEmpty($record) {

        if (!isset($record->first_name) || empty($record->first_name) &&
            !isset($record->last_name) || empty($record->last_name) &&
            !isset($record->prize) || empty($record->prize)
        ) {

            return false;
        }

        return true;
    }
}