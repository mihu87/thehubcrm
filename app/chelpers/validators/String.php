<?php

namespace CHelpers\Validators;

use CHelpers\Handlers\CNP;

class String {

    static public function checkCNP($strCNP) {

        $oCNP = new CNP($strCNP);

        if (empty($oCNP->error)) {
            return false;
        }

        return true;
    }

    static public function emailApiValidation($string) {

        if (strpos($string, '/') !== false) {
            return false;
        }

        $apiLink = 'http://info.6a.ro/api/email/' . $string;

        $httpResponse = file_get_contents($apiLink);

        $response = explode(",", trim($httpResponse));

        return (int) $response[1] > 0 ? true : false;
    }

    static public function phoneNumberApiValidation($string) {

        if (strpos($string, '/') !== false || !is_numeric($string)) {
            return false;
        }

        $apiLink = 'http://info.6a.ro/api/mobile/' . $string;

        $httpResponse = file_get_contents($apiLink);

        $response = explode(",", trim($httpResponse));

        return (int) $response[1] > 0 ? true : false;
    }
}