<?php

use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $this->call('NomenclatureInteractionTypeSeeder');
            $this->command->info('Interaction Type Table Seeded!');



        //$this->call('RawTableTriggerSeeder');
        //$this->command->info('Raw Table Trigger created');

    }

}

class NomenclatureInteractionTypeSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = DB::table('nomenclature_interaction_type');
        $tbl->truncate();

        $tbl->insert([
            [
                'code' => 'activated_by_pin',
                'description' => 'Cont activat in mypass folosind codul primit prin SMS',
                'output_interaction' => 'interaction'
            ],
            [
                'code' => 'banned_for_wrong_codes',
                'description' => 'Cont blocat pana la finalul zilei pentru introducerea a 10 coduri invalide consecutiv',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'change_password',
                'description' => 'Dupa actiunea de touched by one2one, in caz ca consumatorul si-a modificat vreo data in afara de data nasterii.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'changed_data',
                'description' => 'Dupa actiunea de touched by one2one, in caz ca consumatorul declara ca este un alt user, adica isi schimba inclusiv data nasterii. In acest caz, actiunea "deactivated by choice in one2one" care urmeaza dupa aceasta actiune, contine un paramentru consent_signature care contine un png cu semnatura consumatorului. Actiunea vine numai atunci cand exista un cont activ in mypass.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'changed_identity',
                'description' => 'Actiune de confirmare a adresei de email (din emailuri tehnice din one2one si creare cont mypass).',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'confirmed_one2one_signup',
                'description' => 'Userul da click pe link-ul din email-ul de creare cont din 121.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'confirmed_recovery_pin',
                'description' => 'Userul introduce codul de pin primit pentru recuperare cont cu succes.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'deactivated_by_choice_in_one2one',
                'description' => 'Vina imediat dupa "changed identity". Semnaleaza inactivarea contului digital mypass. Consumatorul devine partial dupa aceasta actiune.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'deactivated_by_one2one_email_proof',
                'description' => 'Semnaleaza inactivarea contului digital mypass, in momentul in care un consumator din one2one dovedeste ca detine emailul care este asociat contului. Aceasta actiune se intampla numai in momentul in care isi confirma mailul. Parametrii contin mailul si id-ul userului care a dovedit. Este urmata de actiunea "notified by sms about deactivation"',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'deactivated_by_recovery',
                'description' => 'E debug info pt. Incercari de frauda. Puteti ignora.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'email_confirmation_sent',
                'description' => 'Trimite email tehnic dupa creare cont mypass.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'email_conflict_confirmation_sent',
                'description' => 'Trimite email tehnic dupa ce un consumator declara (in one2one) un email existent al unui cont activ mypass. Daca consumatorul da click pe linkul din acest email in mai putin de 48h, contul activ devine inactiv si se emite "deactivated by one2one email proof" pe acel cont.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'entered_correct_code',
                'description' => 'Cod corect introds',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'entered_used_code',
                'description' => 'Cod deja folosit',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'entered_wrong_code',
                'description' => 'Cod gresit',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'failed_to_log_in',
                'description' => 'Eroare de log in',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'finished_one2one_signup',
                'description' => 'Userul completeaza procesul de creare cont din 121 cu succes.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'log_in',
                'description' => 'Log in cu succes in mypass',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'lost_email_to_register',
                'description' => 'Un consumator din one2one, isi pierde emailul dupa ce un utilizator isi face cont pe mypass si activeaza cu pin.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'maybe_touched_by_one2one',
                'description' => 'Se emite atunci cand BA foloseste "0000" si exista deja un consumator mypass sau one2one (nu e valabil pentru useri SMS) cu acelasi numar de telefon.',
                'output_interaction' => 'interaction'
            ],
            [
                'code' => 'notified_by_sms_about_deactivation',
                'description' => 'Se emite imediat dupa "deactivated by one2one email proof". Userul care are contul inactivat primeste SMS prin care este informat de acest lucru.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'played',
                'description' => 'Engemenent jucat',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'reactivated_after_ecr',
                'description' => 'In caz ca un user activ mypass, vrea sa-si schimbe emailul prin one2one si declara un email care apartine unui alt cont activ mypass, contul lui devine dezactivat pana cand da click pe linkul din mailul asociat actiunii "email conflict confirmation sent". Daca da click pe link, contul devine activ din nou si se emite aceasta actiune.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'registered',
                'description' => 'Completare pasul 3 creare cont mypass. (inainte de activare pin)',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'registered_by_one2one',
                'description' => 'Cont nou prin one2one.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'registered_by_sms',
                'description' => 'Cont nou prin sms.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'requested_recovery_email',
                'description' => 'Trimite email recuperare parola.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'requested_recovery_pin',
                'description' => 'Trimitere SMS recuperare cont (parola)',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'resolved_email_conflict',
                'description' => 'A dat click pe linkul din mailul asociat actiunii "email conflict confirmation sent"',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'sent_one2one_signup_email',
                'description' => 'Email de creare cont in mypass din 121 trimis catre consumator',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'sms',
                'description' => 'Orice mesaj primit de la un participant',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'suspended_after_activation_remains_temp',
                'description' => 'Dupa ce un consumator cu cont de one2one sau SMS isi face cont pe mypass, se creeaza un cont temporar pana la validarea pinului. Dupa validare se emite actiunea asta si poate fi sters si de voi.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'touched_by_one2one',
                'description' => 'Consumator existent atins de one2one (contact cu succes).',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'unsuccessful_bonus_code_entry',
                'description' => 'Cod bonus introdus fara echivalent din pachet',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'upgraded_by_one2one',
                'description' => 'Consumator de SMS atins de one2one',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'upgraded_to_active_regular_user',
                'description' => 'Consumator SMS upgradat la cont activ.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'used_code',
                'description' => 'Debug info, poate fi ignorat. Ar trebui sa fie prezent mereu dupa entered correct code.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'used_code_for_registration',
                'description' => 'Cod folosit pentru creare cont',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'welcome_email_sent',
                'description' => 'Email creare cont in mypass trimis catre user',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'won_prize',
                'description' => 'Castigare premiu',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'imported_for_londoners',
                'description' => 'Cont importat pentru backwards compatibility Londoners.',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'became_londoner',
                'description' => 'Consumatorul tocmai a devenit membru Londoners. Actiunea vine dupa actiunile uzuale one2one si contine parametrii lor (redundant).',
                'output_interaction' => 'none'
            ],
            [
                'code' => 'field_auto_correction',
                'description' => 'Inregistrarea nu a trecut procesul de validare iar campul invalid a fost corectat automat de sistemul curent.',
                'output_interaction' => 'auto_correct'
            ]
          ]
        );

    }

}

class RawTableTriggerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trigger =<<<TRIGGER
        CREATE
            TRIGGER `raw_data_dm_selection_before_insert` BEFORE INSERT
            ON `raw_data_dm_selection`
            FOR EACH ROW BEGIN

                SET NEW.uhash1 = TRIM(LOWER(NEW.uhash1));
                SET NEW.uhash2 = TRIM(LOWER(NEW.uhash2));

               -- SET @MIN_DATES_DIFF = 25569;
               -- SET @SEC_IN_DAY = 86400;

               -- SET NEW.timestamp = ( SELECT IF (NEW.timestamp <= @MIN_DATES_DIFF, 0, FROM_UNIXTIME( (NEW.timestamp - @MIN_DATES_DIFF) * @SEC_IN_DAY, '%Y-%m-%d %h:%i:%s')) );

            END;
TRIGGER;

        DB::unprepared($trigger);
    }
}


