<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;


class UserTableSeeder extends Seeder
{
    public function run()
    {
        $tbl = DB::table('users');
        $tbl->truncate();

        $tbl->insert(
            ['username' => 'cristi.manea', 'password' => Hash::make('cristian0'), 'mobile' => '0720904359', 'email' => 'cristi.manea@thehubpartners.ro']
        );
    }
}