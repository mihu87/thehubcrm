<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NomenclatureUsersLog extends Migration {

    const TABLE_NAME = 'nomenclature_users_log';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('username', 32)->nullable();
            $table->string('session_id', 32)->nullable();
            $table->string('action', 32)->nullable();
            $table->longText('query')->nullable();
            $table->string('import_from', 50)->nullable();
            $table->string('import_result', 50)->nullable();
            $table->string('export_from', 50)->nullable();
            $table->text('export_params')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists(self::TABLE_NAME);
	}

}
