<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtendedInteractionTable extends Migration {

    const TABLE_NAME = 'interactions';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('uuid', 255); //sha1
            $table->string('external_id', 64);
            $table->string('first_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->date('birth_date')->nullable();
            $table->char('gender', 1)->nullable();
            $table->string('cnp', 13)->nullable();
            $table->string('mobile', 16)->nullable();
            $table->string('phone', 16)->nullable();
            $table->string('email', 64)->nullable();
            $table->string('county', 16)->nullable();
            $table->string('city', 16)->nullable();
            $table->string('street_type', 16)->nullable();
            $table->string('street_name', 50)->nullable();
            $table->string('street_no', 8)->nullable();
            $table->string('building', 8)->nullable();
            $table->string('entrance', 8)->nullable();
            $table->string('floor', 8)->nullable();
            $table->string('apartment', 8)->nullable();
            $table->tinyInteger('zip_code', false, true)->nullable();
            $table->tinyInteger('optin_optout', false, true)->nullable();
            $table->string('brand_regular', 30)->nullable();
            $table->string('brand_regular_sku', 20)->nullable();
            $table->string('brand_occasional', 30)->nullable();
            $table->string('brand_occasional_sku', 20)->nullable();
            $table->string('valid_phone')->nullable()->default(0);
            $table->string('valid_email')->nullable()->default(0);
            $table->string('valid_dm')->nullable()->default(0);
            $table->string('action', 64)->nullable();
            $table->string('action_date', 128)->nullable();
            $table->string('campaign_code', 32)->nullable();
            $table->string('campaign_name', 32)->nullable();
            $table->string('communication_channel', 32)->nullable();
            $table->string('communication_sub_channel', 40)->nullable();
            $table->enum('email_bounce', ['sb', 'hb'])->nullable();
            $table->tinyInteger('email_click', false, true)->nullable();
            $table->tinyInteger('email_opened', false, true)->nullable();
            $table->string('claim_code')->nullable();
            $table->string('phone_status', 10)->nullable();
            $table->string('offer_type', 32)->nullable();
            $table->string('coupon_code')->nullable();
            $table->string('dm_status', 10)->nullable();
            $table->integer('ticket_no', false, true)->nullable();
            $table->text('infoline_msg')->nullable();
            $table->string('prize', 128)->nullable();
            $table->string('ba_id', 30)->nullable();
            $table->string('ba_name', 50)->nullable();
            $table->string('pos_code', 16)->nullable();
            $table->string('pos_city', 32)->nullable();
            $table->string('pos_county', 32)->nullable();
            $table->string('interaction_type', 32)->nullable();
            $table->dateTime('timestamp');
            $table->integer('timestamp_unix')->nullable();
            $table->timestamps();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists(self::TABLE_NAME);
	}

}
