<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RawInteractions121 extends Migration {

    const TABLE_NAME = 'raw_interactions_121';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {

            $table->increments('id');
            $table->string('consumer_id')->nullable();
            $table->timestamp('consumer_time')->nullable();
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('cnp')->nullable();
            $table->string('sex')->nullable();
            $table->date('born')->nullable();
            $table->string('address_state')->nullable();
            $table->string('address_city')->nullable();
            $table->string('address_street')->nullable();
            $table->string('address_no')->nullable();
            $table->string('address_building')->nullable();
            $table->string('address_stair')->nullable();
            $table->string('address_floor')->nullable();
            $table->string('address_ap')->nullable();
            $table->string('mobile')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('brand1')->nullable();
            $table->string('brand2')->nullable();
            $table->string('prize')->nullable();
            $table->string('matrix')->nullable();
            $table->string('agreement')->nullable();
            $table->string('questions')->nullable();
            $table->string('pos_pos_code')->nullable();
            $table->string('pos_pos_name')->nullable();
            $table->string('pos_pos_city')->nullable();
            $table->string('ba')->nullable();
            $table->string('campaign_id')->nullable();
            $table->string('campaign_name')->nullable();
            $table->string('campaign_channel')->nullable();
            $table->string('campaign_sub_channel')->nullable();
            $table->integer('status')->nullable()->default(0); // 0 - none , 1 - acquired, 2 - processed
            $table->timestamps();

            $table->index('consumer_time');
            $table->index('status');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists(self::TABLE_NAME);
	}

}
