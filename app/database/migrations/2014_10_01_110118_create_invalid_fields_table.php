<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvalidFieldsTable extends Migration {

    const TABLE_NAME = 'invalid_fields';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {

            $table->increments('id');
            $table->integer('interaction_id');
            $table->string('field', 60);
            $table->integer('status');
            $table->timestamps();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists(self::TABLE_NAME);
	}

}
