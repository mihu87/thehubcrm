<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCodeToDescriptionColumnInNomenclatureInteractionTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE `nomenclature_interaction_type` CHANGE COLUMN `name` `description` TEXT NULL DEFAULT NULL AFTER `code`;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE `nomenclature_interaction_type` CHANGE COLUMN `description` `name` VARCHAR(64) NULL AFTER `code`;');
	}

}
