<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RawDataSmsOutTable extends Migration {

    const TABLE_NAME = 'raw_data_smsout';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {

            $table->increments('id');
            $table->string('consumer_id')->nullable();
            $table->string('mobile')->nullable();
            $table->string('campaign_id')->nullable();
            $table->integer('processed')->nullable()->default(0); // 0 - none , 1 - acquired, 2 - processed
            $table->timestamps();

            $table->index('consumer_id');
            $table->index('mobile');
            $table->index('processed');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists(self::TABLE_NAME);
	}

}
