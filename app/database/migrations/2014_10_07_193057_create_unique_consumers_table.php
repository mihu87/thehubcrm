<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniqueConsumersTable extends Migration {

    const TABLE_NAME = 'consumers';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('external_id', 20);
            $table->string('uuid', 255)->unique(); //sha1
            $table->string('first_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->date('birth_date')->nullable();
            $table->char('gender', 1)->nullable();
            $table->string('cnp', 13)->nullable();
            $table->string('mobile', 16)->nullable();
            $table->string('email', 64)->nullable();
            $table->string('county', 16)->nullable();
            $table->string('city', 16)->nullable();
            $table->string('street_type', 16)->nullable();
            $table->string('street_name', 50)->nullable();
            $table->string('street_no', 8)->nullable();
            $table->string('building', 8)->nullable();
            $table->string('entrance', 8)->nullable();
            $table->string('floor', 8)->nullable();
            $table->string('apartment', 8)->nullable();
            $table->tinyInteger('zip_code', false, true)->nullable();
            $table->tinyInteger('optin_optout', false, true)->nullable();
            $table->string('brand_regular', 30)->nullable();
            $table->string('brand_regular_sku', 20)->nullable();
            $table->dateTime('first_seen');
            $table->dateTime('last_seen');
            $table->string('phone_status')->nullable()->default(1);
            $table->string('email_status')->nullable()->default(0);
            $table->string('dm_status')->nullable()->default(1);
            $table->timestamps();

        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists(self::TABLE_NAME);
	}

}
