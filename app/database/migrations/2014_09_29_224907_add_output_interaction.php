<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class AddOutputInteraction extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("ALTER TABLE  `nomenclature_interaction_type` ADD  `output_interaction` VARCHAR( 25 ) NOT NULL DEFAULT  'none' AFTER  `description`");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement("ALTER TABLE `nomenclature_interaction_type` DROP `output_interaction`;");
	}

}
