<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NomenclatureSkuBrands extends Migration {

    const TABLE_NAME = 'nomenclature_sku_brands';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {

            $table->increments('id');
            $table->string('sku_code', 32)->nullable();
            $table->string('sku_name', 128)->nullable();
            $table->string('sku_brand', 128)->nullable();
            $table->string('sku_vendor',128)->nullable();
            $table->string('manufacture',128)->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists(self::TABLE_NAME);
	}

}
