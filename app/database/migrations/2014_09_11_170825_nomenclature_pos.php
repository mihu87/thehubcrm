<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NomenclaturePos extends Migration {

    const TABLE_NAME = 'nomenclature_pos';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {

            $table->increments('id');
            $table->string('code', 32);
            $table->string('name', 64)->nullable();
            $table->string('city', 128)->nullable();
            $table->string('county', 128)->nullable();
            $table->string('region_code', 32)->nullable();
            $table->string('region', 128)->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists(self::TABLE_NAME);
	}

}
