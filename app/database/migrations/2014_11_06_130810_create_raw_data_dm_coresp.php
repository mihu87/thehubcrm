<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRawDataDmCoresp extends Migration {

    const TABLE_NAME = 'raw_data_dm_coresp';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {

            $table->increments('id');
            $table->string('consumer_id')->nullable();
            $table->string('timestamp')->nullable();
            $table->string('campaign_id')->nullable();
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('county')->nullable();
            $table->string('city')->nullable();
            $table->string('Street')->nullable();
            $table->string('no')->nullable();
            $table->string('building')->nullable();
            $table->string('entrance')->nullable();
            $table->string('floor')->nullable();
            $table->string('apartment')->nullable();
            $table->string('Cod')->nullable();
            $table->integer('processed')->nullable()->default(0); // 0 - none , 1 - acquired, 2 - processed
            $table->timestamps();

            $table->index('timestamp');
            $table->index('Cod');
            $table->index('processed');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists(self::TABLE_NAME);
	}

}
