<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRawDataSmsSelectionTable extends Migration {

    const TABLE_NAME = 'raw_data_sms_selection';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {

            $table->increments('id');
            $table->string('participant')->nullable();
            $table->string('timestamp')->nullable();
            $table->string('campaign_id')->nullable();

            $table->integer('processed')->nullable()->default(0); // 0 - none , 1 - acquired, 2 - processed
            $table->timestamps();

            $table->index('participant');
            $table->index('processed');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists(self::TABLE_NAME);
	}

}
