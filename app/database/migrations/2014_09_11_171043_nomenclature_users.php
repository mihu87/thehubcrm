<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NomenclatureUsers extends Migration {

    const TABLE_NAME = 'nomenclature_users';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {

            $table->increments('id');
            $table->string('username', 32);
            $table->string('password', 32);
            $table->string('full_name', 32)->nullable();
            $table->enum('user_level', ['user','superadmin']);
            $table->dateTime('last_login')->nullable();
            $table->timestamps();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists(self::TABLE_NAME);
	}

}
