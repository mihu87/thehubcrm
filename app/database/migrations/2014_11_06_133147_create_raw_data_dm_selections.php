<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateRawDataDmSelections extends Migration {

    const TABLE_NAME = 'raw_data_dm_selection';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {

            $table->engine = 'InnoDb';

            $table->string('consumer_id')->nullable()->default(null);
            $table->string('timestamp')->nullable()->default(null);
            $table->string('campaign_id')->nullable()->default(null);
            $table->string('fname')->nullable()->default(null);
            $table->string('lname')->nullable()->default(null);
            $table->string('county')->nullable()->default(null);
            $table->string('city')->nullable()->default(null);
            $table->string('street')->nullable()->default(null);
            $table->string('no')->nullable()->default(null);
            $table->string('building')->nullable()->default(null);
            $table->string('entrance')->nullable()->default(null);
            $table->string('floor')->nullable()->default(null);
            $table->string('apartment')->nullable()->default(null);
            $table->string('uhash1')->nullable()->default(null);
            $table->string('uhash2')->nullable()->default(null);

            $table->increments('id');
            $table->integer('processed')->nullable()->default(0); // 0 - none , 1 - acquired, 2 - processed
            $table->timestamps();

            $table->index('timestamp');
            $table->index('processed');



        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists(self::TABLE_NAME);
	}

}
