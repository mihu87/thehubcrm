<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Services\DataContext;

class ProcessCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'process:consume';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        gc_enable();

        $source = $this->option('name');

        $context = new DataContext;
        $context->setConsole($this);
        $context->setSource($source);

        if ($context->getSourceType() == 'selection') {

            $toProcess = DB::table($context->getSourceRawTableName())->where('processed', 0)->orderBy('timestamp', 'ASC')->get();

            foreach ($toProcess as $record) {
                $outResult = $context->processIn($record);
            }

        } else {

            /*var_dump($context->getSourceRawTableName());
            $toProcess = DB::table('raw_interactions_121')->where('status', '=', 0)->orderBy('consumer_time', 'ASC');
            $blk = 1000;
            $start = microtime(true);
            $i = 0;
            $toProcess->chunk($blk, function($chunk) use ($context, $blk, $start, $i) {
                foreach ($chunk as $record) {
                    $outResult = $context->process($record);
                }
            });*/

            $this->dbh = new PDO("mysql:host=localhost;dbname=crm_dev","root","123root456");
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $res = $this->dbh->query("SELECT * FROM " . $context->getSourceRawTableName());
            //$rows = $res->fetchColumn(0);
            //var_dump($rows);

            $blk = 1000;

            $start = microtime(true);
            $i = 0;

            while ($row = $res->fetch(PDO::FETCH_OBJ)) {

                $outResult = $context->process($row);

                $stmt = $this->dbh->prepare("UPDATE " . $context->getSourceRawTableName() . " SET status = 1 WHERE id = :id");
                $stmt->bindParam(':id', $row->id, PDO::PARAM_INT);
                $stmt->execute();


                if ($i % $blk == 0) {
                    $end = microtime(true);
                    $delta = ($end-$start)*1000;
                    $perf = $delta/$blk;
                    echo "$i: $delta ms per $blk recs, $perf ms per record\n";
                    $start = microtime(true);
                }

                ++$i;
            }



        }


        /*
        $handle = fopen('./app/external/import-sql.csv', "rb");

        $headers = [
            'id',
            'consumer_id',
            'consumer_time',
            'fname',
            'lname',
            'cnp',
            'sex',
            'born',
            'address_state',
            'address_city',
            'address_street',
            'address_no',
            'address_building',
            'address_stair',
            'address_floor',
            'address_ap',
            'mobile',
            'phone',
            'email',
            'brand1',
            'brand2',
            'prize',
            'matrix',
            'agreement',
            'questions',
            'pos_pos_code',
            'pos_pos_name',
            'pos_pos_city',
            'ba',
            'campaign_id',
            'campaign_name',
            'campaign_channel',
            'campaign_sub_channel',
            'status'
        ];

        if ($handle) {

            $this->info("Memory usage before: " . (memory_get_usage() / 1024) . " KB");

            $i = 0;

            $start = microtime(true);

	        $blk = 100;

            while (($line = fgets($handle)) !== false) {

                $line = explode("|", $line);

                if (count($headers) == count($line)) {
                    $lineArray = array_combine($headers, $line);
                    $outResult = $context->process($lineArray);

                } else {
                    echo var_export($line, true) .  "(( $i ))" . "\n";
                }

                if ($i % $blk == 0) {
                   $end = microtime(true);
                   $delta = ($end-$start)*1000;
                   $perf = $delta/$blk;
                   echo "$i: $delta ms per $blk recs, $perf ms per record\n";
                   $start = microtime(true);
                }

                echo $i . "\r";

                ++$i;

            }

            fclose($handle);

            $end = (microtime(true) - $start);

            $this->info("elapsed time: $end seconds");
            $this->info("Total number of lines : $i");
            $this->info("Memory usage after sleep: " . (memory_get_usage() / 1024) . " KB");


        }
        */


	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::OPTIONAL, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
        return array(
            array('name',
                'sn',
                InputOption::VALUE_REQUIRED,
                'The source you want to consume queued records.',
                null
            ),
        );
	}


}


/*
 *
 * $left = array_sum($A);

   $right = 0;

   $smallest = $left;

   $index = count($A);

   $difference = false;

   while ($index--) {
    $right += $A[$index];
    $left += $A[$index];
    $difference = abs($right-$left);

    if ($difference < $smallest) {
        $smallest = $difference;
    }
    return $smallest;
   }
 */

/*
 *
  LOAD DATA LOCAL INFILE '/var/www/html/crm/scripts/report.csv'
  INTO TABLE report_old_script FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' (action, consumer_id) SET id = 0;
 */