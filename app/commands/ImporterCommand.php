<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Services\DataContext;

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

use Illuminate\Support\Facades\Session;

class ImporterCommand extends Command {

    /** @var  $connection AMQPConnection */
    protected $connection;

    /** @var  $queueName */
    protected $queueName;

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'remote:importer';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'This module is developed for importing remote data.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $action = $this->option('action');
        $test = $this->option('test');

        if (empty($action)) {
            $action = $this->ask('Would you like to run or create a source? [run|create]');
        }

        if ($test) {
            Session::put('test_case', $test);
            $this->info('Test mode is on on for case : ' . $test);
        }

        $sourceName = $this->determineSourceName($action);

        switch ($action) {
            case 'run' :
                    $this->runImport($sourceName, $test);
                break;

            case 'create' :
                    $this->createSource($sourceName, $test);
                break;

            default:
                break;
        }


	}

    protected function runImport($source) {

        $first  = new DateTime();

        $output = new Symfony\Component\Console\Output\ConsoleOutput();

        $context = new DataContext;
        $context->setConsole($this);
        $context->setSource($source);

        /** @var  $data AppendIterator */

        $data = $context->download();

        /*
        $totalObjects = iterator_count($data);

        $this->info("[x] Downloading content...");

        //open RabbitMq Channel
        $this->openConnection($source);

        $progress = new \Symfony\Component\Console\Helper\ProgressBar($output, $totalObjects);
        $progress->start();
        $progress->setBarWidth(40);
        $progress->setFormat("%current%/%max% [%bar%] | Elapsed: %elapsed:6s% | ~Remaining: %estimated:-6s% | Mem: %memory:6s% %percent:3s%%");

        foreach ($data as $record) {
            $this->publish($record);
            $progress->advance();
        }

        $progress->finish();

        // Close RabbitMq Channel
        $this->closeConnection();
        */
    }

    /*protected function openConnection($source) {

        $this->queueName = 'process_' . strtolower($source);

        $this->connection = new AMQPConnection('localhost', 5672, 'guest', 'guest');
        $this->connection
            ->channel()
            ->queue_declare($this->queueName, false, true, false, false);

    }

    protected function publish($message) {

        $comp = json_encode($message);

        $this->connection
            ->channel()
            ->basic_publish(new AMQPMessage($comp, array('delivery_mode' => 2)), '', $this->queueName);

    }

    protected function closeConnection() {

        $this->connection->channel()->close();
        $this->connection->close();
    }*/

    protected function createSource($source) {

        $dirName = 'RS' . $source;

        if (is_numeric($source)) {
            $this->error('I cannot allow you to use numeric character for Source name :( . It\'s not about me, sew PHP...');
            return false;
        }

        if (file_exists('./app/services/sources/' . $dirName)) {

            $this->error('Directory ' . $dirName . ' already exists. Cannot overwrite!');
            return false;
        }

        $configFiles = [
            'Downloader.php' => str_replace("#name#", $dirName, file_get_contents('./app/services/sources/source-tpl/Downloader.tpl')),
            'Translator.php' => str_replace("#name#", $dirName, file_get_contents('./app/services/sources/source-tpl/Translator.tpl')),
            'Normalizer.php' => str_replace("#name#", $dirName, file_get_contents('./app/services/sources/source-tpl/Normalizer.tpl')),
            'Injector.php' => str_replace("#name#", $dirName, file_get_contents('./app/services/sources/source-tpl/Injector.tpl')),
            'Junker.php' => str_replace("#name#", $dirName, file_get_contents('./app/services/sources/source-tpl/Junker.tpl')),
            'Validator.php' => str_replace("#name#", $dirName, file_get_contents('./app/services/sources/source-tpl/Validator.tpl')),
            'Evaluator.php' => str_replace("#name#", $dirName, file_get_contents('./app/services/sources/source-tpl/Evaluator.tpl')),
        ];

        $sourceClass = [$source . '.php' => str_replace(["#name#", '#namespace#'], [$source, $dirName], file_get_contents('./app/services/sources/source-tpl/#name#.tpl'))];


        $createDir = mkdir('./app/services/sources/' . $dirName . '/Config', 0777, true);

        if (!$createDir) return false;

        foreach ($configFiles as $fileName => $fileContent) {
            file_put_contents('./app/services/sources/' . $dirName . '/Config/' . $fileName, $fileContent);
        }

        foreach ($sourceClass as $fileName => $fileContent) {
            file_put_contents('./app/services/sources/' . $dirName . '/' . $fileName, $fileContent);
        }

        $this->info('Directory/File structure created.');

        $this->info('Now, we need to wait a little for artisan to generate auto-load files... :)');
        \Illuminate\Support\Facades\Artisan::call('dump-autoload');

        $this->info('DONE!');

    }

    protected function determineSourceName($action) {

        $name = null;

        if (!empty($action)) {
            $name = $this->option('name');

            if (empty($name)) {
                $name = $this->ask('Please input source name to ' . $action);
            }
        }

        return $name;
    }


	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('run', InputArgument::OPTIONAL, false),
			array('create-source', InputArgument::OPTIONAL, false),

		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('name',
                'sn',
                InputOption::VALUE_REQUIRED,
                'The source you want to import data from | use "ALL" for all Sources or [name] for an existing source.',
                null
            ),
			array('action', 'a', InputOption::VALUE_OPTIONAL, 'What do you want to do (create or run)?', null),
			array('test', 't', InputOption::VALUE_OPTIONAL, 'Test Mode?', null),
		);
	}


    protected function progressBar($current, $total, $label)
    {
        // This function assumes that you start with completion of 0%.

        // If the first time you call this function is with 1%
        // completion, you will delete the last 106 characters of
        // output from your program.

        // If this is the case, simply call this function before with
        // a hard coded 0.

        // check to see if this is the first go-round
        if ($current == 0)
        {
            // this is the first time so output the progess bar label
            if ($label == "")
                echo "Progress: ";
            else if ($label != "none")
                echo $label;

            // start the bar with a nice edge
            echo "|";
        }
        else
        {
            // this isn't the first time so remove the previous progress bar
            for ($place = 106; $place >= 0; $place--)
            {
                // echo a backspace to remove the previous character
                echo "\010";
            }
        }

        // output the progess bar as it should be
        for ($place = 0; $place <= 100; $place++)
        {
            // output stars if we're finished through this point
            // or spaces if not
            if ($place <= ($current / $total * 100))
                echo "*";
            else
                echo " ";
        }

        // end the bar with a nice edge and a label
        echo "| 100%";

        // check to see if this is the last go-round
        if ($current == $total)
        {
            // this is the end of the progress bar, output an end of line
            echo "\n";
        }
    }

}
