<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use PDO;


class WebCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'web:xml';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

        $activities = [
            ':action1'=> 'maybe touched by one2one',
            ':action2'=> 'registered by one2one',
            ':action3' =>'touched by one2one',
            ':action4' =>'upgraded by one2one'
        ];
        //'action=:action1 or action=:action2 or action=:action3 or action=:action4'
        $this->dbh = new PDO("mysql:host=localhost;dbname=dbdmf","root","123root456");
        $stmt = $this->dbh->prepare("SELECT id, activity_id, consumer_id, FROM_UNIXTIME(timestamp) as timestamp_hr, date_created, campaign_id, action, chan, user_agent, params FROM activity_log a JOIN consumer c ON c.consumer_id = a.consumer_id WHERE (action=:action1 or action=:action2 or action=:action3 or action=:action4) ORDER BY timestamp desc");

        foreach ( $activities as $k => $v ) {
            $stmt->bindParam($k, $v, PDO::PARAM_STR);
        }

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($rows as $row) {
            var_dump($row);
        }
    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
