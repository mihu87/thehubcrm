<?php



class BaseModel extends \Eloquent {

    public static function boot()
    {
        parent::boot();

        static::creating(function($model){

            $model->uuid = $model->first_name.$model->last_name.$model->birth_date.$model->mobile;
            $model->timestamp_unix = (strtotime($model->timestamp) == true ? strtotime($model->timestamp) : null);

            $headerCount = Consumer::where('uuid', $model->uuid)->count();

            if ($headerCount > 0) {

                $consumer = Consumer::where('uuid', $model->uuid)->first();

                switch ($model->interaction_type) {

                    case 'email_change' :
                            $consumer = Consumer::find($consumer->id);
                            $consumer->email = $model->email;
                            $consumer->save();
                    break;

                    case 'brand_change' :
                            $consumer = Consumer::find($consumer->id);
                            $consumer->brand_regular = $model->brand_regular;
                            $consumer->save();
                        break;

                    case 'address_change' :
                            $consumer = Consumer::find($consumer->id);
                            $consumer->county = $model->county;
                            $consumer->city = $model->city;
                            $consumer->street_type = $model->street_type;
                            $consumer->street_name = $model->street_name;
                            $consumer->street_no = $model->street_no;
                            $consumer->building = $model->building;
                            $consumer->entrance = $model->entrance;
                            $consumer->floor = $model->floor;
                            $consumer->apartment = $model->apartment;
                            $consumer->save();
                        break;

                    default :
                        $consumer = Consumer::find($consumer->id);
                        $consumer->last_seen = $model->timestamp;
                        $consumer->save();
                        break;
                }

            }

        });
    }

    public function getColumnNames()
    {
        switch (DB::connection()->getConfig('driver')) {
            case 'pgsql':
                $query = "SELECT column_name FROM information_schema.columns WHERE table_name = '".$this->table."'";
                $column_name = 'column_name';
                $reverse = true;
                break;

            case 'mysql':
                $query = 'SHOW COLUMNS FROM '.$this->table;
                $column_name = 'Field';
                $reverse = false;
                break;

            case 'sqlsrv':
                $parts = explode('.', $this->table);
                $num = (count($parts) - 1);
                $table = $parts[$num];
                $query = "SELECT column_name FROM ".DB::connection()->getConfig('database').".INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'".$table."'";
                $column_name = 'column_name';
                $reverse = false;
                break;

            default:
                $error = 'Database driver not supported: '.DB::connection()->getConfig('driver');
                throw new Exception($error);
                break;
        }

        $columns = array();

        foreach(DB::select($query) as $column)
        {
            $columns[] = $column->$column_name;
        }

        if($reverse)
        {
            $columns = array_reverse($columns);
        }
        return $columns;
    }
}