<?php
use Illuminate\Database\Eloquent\Model;
class RemoteDmfActivityLog extends Eloquent {

    protected $connection = 'mysql_dmf_local';
    protected $table = 'activity_log';

    public function activity() {
        return $this->belongsTo('RemoteDmfConsumers', 'consumer.consumer_id', 'activity_log.consumer_id');
    }
}