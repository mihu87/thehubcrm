<?php

class Interaction extends \BaseModel {

    protected $table = 'interactions';

    public static function boot()
    {
        parent::boot();
    }

    /* Remove an attribute from the model.
	 *
	 * @param  string  $key
	 */
    final public function purge($key)
    {
        unset($this->original[$key]);
        unset($this->attributes[$key]);
    }
}