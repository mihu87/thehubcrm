<?php


class RemoteDmfConsumers extends Eloquent {

    protected $connection = 'mysql_dmf_local';
    protected $table = 'consumers';

    public static function getInteractions() {

        $DB = DB::connection('mysql_dmf_local');
        $rst = $DB->select('SELECT activity_log.id as activity_log_id, activity_log.consumer_id as consumer_id, activity_log.*, consumers.* FROM consumers LEFT JOIN activity_log ON activity_log.consumer_id = consumers.id WHERE consumers.id IN (225317, 225318, 225319)');

        return $rst;
    }
}