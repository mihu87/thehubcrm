<?php

class ConsumerJunk extends \Eloquent {

    protected $table = 'consumers_junk';
    protected $fillable = [
        'external_id',
        'uuid',
        'first_name',
        'last_name',
        'birth_date',
        'gender',
        'cnp',
        'mobile',
        'email',
        'county',
        'city',
        'street_type',
        'street_name',
        'street_no',
        'building',
        'entrance',
        'floor',
        'apartment',
        'zip_code',
        'optin_optout',
        'brand_regular',
        'brand_regular_sku',
        'first_seen',
        'last_seen',
        'valid_phone',
        'valid_email',
        'valid_dm'
    ];

    public function getFillable() {
        return $this->fillable;
    }

    /*public static function boot()
    {
        parent::boot();

        static::creating(function($model){
              $model->uuid = sha1($model->exernal_id . $model->first_seen);
        });
    }*/
}