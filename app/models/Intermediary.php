<?php

use Jenssegers\Mongodb\Model as Interim;

class Intermediary extends Interim {

    protected $collection = 'raw_consumers';
    protected $connection = 'mongodb';
    protected $guarded = array();
}