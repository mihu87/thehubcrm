<?php

/**
 * Films model config
 */

return array(

    'title' => 'Raw XML 121 Interactions',

    'single' => 'raw_interactions_121',

    'model' => 'RawInteraction121',

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'consumer_id',
        'consumer_time',
        'fname',
        'lname',
        'cnp',
        'sex',
        'born',
/*        'address_state',
        'address_city',
        'address_street',
        'address_no',
        'address_building',
        'address_stair',
        'address_floor',
        'address_ap',*/
        'mobile',
        'phone',
        'email',
        /*'brand1',
        'brand2',
        'prize',
        'matrix',
        'agreement',
        'questions',
        'pos_pos_code',
        'pos_pos_name',
        'pos_pos_city',
        'ba',
        'campaign_id',
        'campaign_name',
        'campaign_channel',
        'campaign_sub_channel',*/
        'status',
        'created_at',
        'updated_at',
/*        'name',
        'release_date' => array(
            'title' => 'release date'
        ),
        'director_name' => array(
            'title' => 'Director Name',
            'relationship' => 'director',
            'select' => "CONCAT((:table).first_name, ' ', (:table).last_name)"
        ),
        'num_actors' => array(
            'title' => '# Actors',
            'relationship' => 'actors',
            'select' => "COUNT((:table).id)"
        ),
        'box_office' => array(
            'title' => 'Box Office',
            'relationship' => 'boxOffice',
            'select' => "CONCAT('$', FORMAT(SUM((:table).revenue), 2))"
        ),*/
    ),

    /**
     * The filter set
     */
    'filters' => array(
        /*
       'id',
       'uuid'
      'name',
       'release_date' => array(
           'title' => 'Release Date',
           'type' => 'date',
           'date_format' => 'yy-mm-dd',
       ),
       'director' => array(
           'title' => 'Director',
           'type' => 'relationship',
           'name_field' => 'name',
           'options_sort_field' => "CONCAT(first_name, ' ' , last_name)",
       ),
       'actors' => array(
           'title' => 'Actors',
           'type' => 'relationship',
           'name_field' => 'name',
           'options_sort_field' => "CONCAT(first_name, ' ' , last_name)",
       ),*/
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'consumer_id',
        'consumer_time',
        'fname',
        'lname',
        'cnp',
        'sex',
        'born',
        'address_state',
        'address_city',
        'address_street',
        'address_no',
        'address_building',
        'address_stair',
        'address_floor',
        'address_ap',
        'mobile',
        'phone',
        'email',
        'brand1',
        'brand2',
        'prize',
        'matrix',
        'agreement',
        'questions',
        'pos_pos_code',
        'pos_pos_name',
        'pos_pos_city',
        'ba',
        'campaign_id',
        'campaign_name',
        'campaign_channel',
        'campaign_sub_channel',
        'status',
//        'release_date' => array(
//            'title' => 'Release Date',
//            'type' => 'date',
//            'date_format' => 'yy-mm-dd',
//        ),
//        'director' => array(
//            'title' => 'Director',
//            'type' => 'relationship',
//            'name_field' => 'name',
//            'options_sort_field' => "CONCAT(first_name, ' ' , last_name)",
//        ),
//        'actors' => array(
//            'title' => 'Actors',
//            'type' => 'relationship',
//            'name_field' => 'name',
//            'options_sort_field' => "CONCAT(first_name, ' ' , last_name)",
//        ),
//        'theaters' => array(
//            'title' => 'Theater',
//            'type' => 'relationship',
//            'name_field' => 'name',
//        ),
    ),

);