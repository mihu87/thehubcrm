<?php

use Kalnoy\Cruddy\Schema\BaseSchema;
use Kalnoy\Cruddy\Service\Validation\FluentValidator;

class ConsumerSchema extends BaseSchema {

    protected $model = 'Consumer';

    /**
     * The name of the column that is used to convert a model to a string.
     *
     * @var string
     */
    protected $titleAttribute = null;

    /**
     * The name of the column that will sort data by default.
     *
     * @var string
     */
    protected $defaultOrder = null;

    /**
     * Define some fields.
     *
     * @param \Kalnoy\Cruddy\Schema\Fields\InstanceFactory $schema
     */
    public function fields($schema)
    {
        $schema->increments('id');
        $schema->string('external_id');
        $schema->string('uuid');
        $schema->string('first_name');
        $schema->string('last_name');
        $schema->date('birth_date');
        $schema->string('gender');
        $schema->string('cnp');
        $schema->string('mobile');
        $schema->email('email');
        $schema->string('county');
        $schema->string('city');
        $schema->string('street_type');
        $schema->string('street_name');
        $schema->string('street_no');
        $schema->string('building');
        $schema->string('entrance');
        $schema->string('floor');
        $schema->string('apartment');
        $schema->string('zip_code');
        $schema->string('optin_optout');
        $schema->string('brand_regular');
        $schema->string('brand_regular_sku');
        $schema->string('first_seen');
        $schema->string('last_seen');
        $schema->string('phone_status');
        $schema->string('email_status');
        $schema->string('dm_status');
        $schema->string('created_at');
        $schema->string('updated_at');

       // $schema->timestamps();
    }

    /**
     * Define some columns.
     *
     * @param \Kalnoy\Cruddy\Schema\Columns\InstanceFactory $schema
     */
    public function columns($schema)
    {
        $schema->col('id');
        $schema->col('external_id');
        $schema->col('uuid');
        $schema->col('first_name');
        $schema->col('last_name');
        $schema->col('birth_date');
        $schema->col('gender');
        $schema->col('cnp');
        $schema->col('mobile');
        $schema->col('email');
        $schema->col('county');
        $schema->col('city');
        $schema->col('street_type');
        $schema->col('street_name');
        $schema->col('street_no');
        $schema->col('building');
        $schema->col('entrance');
        $schema->col('floor');
        $schema->col('apartment');
        $schema->col('zip_code');
        $schema->col('optin_optout');
        $schema->col('brand_regular');
        $schema->col('brand_regular_sku');
        $schema->col('first_seen');
        $schema->col('last_seen');
        $schema->col('phone_status');
        $schema->col('email_status');
        $schema->col('dm_status');
        $schema->col('created_at');
        $schema->col('updated_at');


       // $schema->col('updated_at')->reversed();
    }

    /**
     * Define some files to upload.
     *
     * @param \Kalnoy\Cruddy\Repo\Stub $repo
     */
    public function files($repo)
    {

    }

    /**
     * Define validation rules.
     *
     * @param \Kalnoy\Cruddy\Service\Validation\FluentValidator $v
     */
    public function rules($v)
    {
        $v->always(
        [

        ]);
    }
}