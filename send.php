<?php

gc_enable();

$input = stream_get_contents(STDIN);
$files = unserialize(base64_decode($input));

foreach ($files as $file) {

    $base = basename($file);
    $tempName = 'temp/' . $base;
    $tempCsv = 'tempcsv/' . $base . '.csv';

    $content = str_replace('&', '-', file_get_contents($file));
    $content = str_replace(',', '', $content);
    $content = utf8_encode($content);

    file_put_contents($tempName, $content, FILE_BINARY);

    system("xsltproc foo.xsl {$tempName} >> {$tempCsv}", $resultSet);

    //system("mysql -uroot -proot -D thp_crm_dev -e \"set foreign_key_checks=0; set sql_log_bin=0; set unique_checks=0; load data local infile '$tempCsv' into table raw_interactions_121 fields terminated by '|' lines terminated by '\n';\" > /dev/null &");

    file_put_contents('send.log', $file .  ' - ' .  memory_get_usage(true) . "\n", FILE_APPEND | LOCK_EX);

    system("rm -f {$tempName}");

    unset($file, $content, $tempName, $tempCsv, $memory, $base);

    gc_collect_cycles();
}


unset($files);

fclose(STDIN);

gc_collect_cycles();

gc_disable();